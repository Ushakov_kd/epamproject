<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<ctg:paginationInit capacity="6" list="${requestScope.ordered_songs}"/>
<fmt:bundle basename="local">
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link href="css/top.css" rel="stylesheet" type="text/css">
        <link href="css/customer.css" rel="stylesheet" type="text/css">
        <link href="css/credit_card.css" rel="stylesheet" type="text/css">
        <link href="css/discount.css" rel="stylesheet" type="text/css">
        <script src="js/jquery.min.js"></script>
        <title><fmt:message key="orders"/></title>
    </head>
    <jsp:include page="/WEB-INF/jsp/parts/top_customer.jsp"/>
    <body>
    <div class="base">
        <div class="songs-block">
            <div class="songs_title">
                <fmt:message key="good_choice"/>
            </div>
            <div class="songs">
                <div class="left_options_column">
                    <c:forEach var="element" items="${requestScope.ordered_songs}" begin="${startElement}"
                               end="${endElement}">
                        <form action="Controller" method="get">
                            <input type="hidden" name="command" value="delete_ordered_song"/>
                            <input type="hidden" name="song_id" value="${element.id}"/>
                            <input type="hidden" name="page" value="${pageContext.request.servletPath}"/>
                            <button class="garbage_button">
                                <fmt:message key="symbol.garbage"/>
                            </button>
                        </form>
                    </c:forEach>
                </div>
                <div class="line_stack">
                    <c:if test="${empty requestScope.ordered_songs}">
                        <p class="error_message"> <fmt:message key="order_has_not_songs"/></p>
                    </c:if>
                    <form action="Controller" method="get">
                        <c:forEach var="element" items="${requestScope.ordered_songs}" begin="${startElement}"
                                   end="${endElement}">
                            <div class="info_line">
                                <input type="hidden" name="command" value="song_page"/>
                                <input type="hidden" name="song_id" value="${element.id}"/>
                                <button class="element_button">
                                        ${element.artist} - ${element.title}
                                </button>
                                <div class="element_info">
                                    <p>${element.price}$</p>
                                </div>
                            </div>
                        </c:forEach>
                    </form>
                </div>
                <ctg:paginationButtons capacity="${capacity}" currentPage="${pageIterator}"
                                       quantity="${entitiesNumber}"/>
                <c:if test="${not empty requestScope.ordered_songs}">
                    <div class="price_block">
                        <div class="total_song">
                            <fmt:message key="total_price"/> ${requestScope.order_total_price}$
                        </div>
                    </div>
                </c:if>
            </div>
        </div>
        <c:if test="${not empty requestScope.ordered_songs}">
            <jsp:include page="/WEB-INF/jsp/parts/block_credit_card.jsp"/>
        </c:if>
        <c:if test="${requestScope.order_total_price != 0}">
            <c:if test="${sessionScope.user.discount != 0}">
                <jsp:include page="/WEB-INF/jsp/parts/block_discount.jsp"/>
            </c:if>
        </c:if>
        <script>
            $(function () {
                $('.garbage_button').click(function () {
                    return window.confirm(<fmt:message key="are_you_sure"/>);
                });
            });
        </script>
    </div>
    </body>
    </html>
</fmt:bundle>