<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="local">
    <fmt:message key="placeholder.card_number" var="card_number"/>
    <fmt:message key="placeholder.card_month" var="month"/>
    <fmt:message key="placeholder.card_year" var="year"/>
    <fmt:message key="placeholder.card_cvc" var="cvc"/>
    <div class="pay-block">
        <form action="Controller" method="post">
            <input type="hidden" name="command" value="pay_order"/>
            <input type="hidden" name="user_id" value="${sessionScope.user.id}"/>

            <input type="text" name="card_number" class="card_number" value=""
                   placeholder="${card_number}" required pattern="[0-9]{12}"/>
            <div class="expiration_date">
                <input type="text" name="card_month" class="card_month" value="" placeholder="${month}" required
                       pattern="(0[1-9])|(1[0-2]){1}">/
                <input type="text" name="card_year" class="card_year" value="" placeholder="${year}" required
                       pattern="[0-9]{2}">
            </div>
            <input type="text" name="card_cvc" class="cvc" value="" placeholder="${cvc}" required
                   pattern="[0-9]{3}"/>

            <button class="buy-button">
                <fmt:message key="button.buy"/>
            </button>
        </form>
    </div>
</fmt:bundle>