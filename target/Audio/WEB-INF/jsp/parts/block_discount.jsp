<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="discount" value="${user.discount}"/>
<c:set var="price" value="${order_total_price}"/>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:formatNumber var="discount_price" value="${price*((100-discount)/100)}"
                  maxFractionDigits="2" />
<fmt:bundle basename="local">
<div class="discount_block">
    <div class="discount_name">
        <fmt:message key="your_own_discount"/>
    </div>
    <div class="discount_value">
        ${discount}%
    </div>
    <div class="discount_name">
        <fmt:message key="your_discount_price"/>
    </div>
    <div class="discount_price">
        ${discount_price}$
    </div>
</div>
</fmt:bundle>