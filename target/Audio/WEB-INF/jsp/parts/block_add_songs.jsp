<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="local">
    <div class="order-block">
        <div class="order">
                <%--@declare id="checked_boxes"--%>
            <div class="selected-songs">
                <p id="result"><span>0</span> <fmt:message key="songs_selected"/></p>
            </div>
            <button form="checked_boxes" class="order-button">
              <fmt:message key="order"/>
            </button>
        </div>
    </div>
</fmt:bundle>