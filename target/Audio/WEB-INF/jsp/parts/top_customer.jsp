<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="local">
    <div class="top">
        <div class="button_space">
            <form action="Controller" method="get">
                <input type="hidden" name="command" value="customer_main_page"/>
                <button class="main-page-button">iAudio</button>
            </form>
            <div class="dropdown">
                <button class="drop-button">
                    <fmt:message key="language"/>
                </button>
                <div class="dropdown-content">
                    <form action="Controller" method="get" class="locale_form">
                        <input type="hidden" name="command" value="set_locale"/>
                        <input type="hidden" name="locale" value="en"/>
                        <input type="hidden" name="previousQuery" value="${pageContext.request.queryString}"/>
                        <button class="locale_button">En</button>
                    </form>
                    <form action="Controller" method="get" class="locale_form">
                        <input type="hidden" name="command" value="set_locale"/>
                        <input type="hidden" name="locale" value="ru"/>
                        <input type="hidden" name="previousQuery" value="${pageContext.request.queryString}"/>
                        <button class="locale_button">Ru</button>
                    </form>
                </div>
            </div>
            <form action="Controller" method="get">
                <input type="hidden" name="command" value="logout"/>
                <button class="top-button">
                    <fmt:message key="logout"/>
                </button>
            </form>
            <form action="Controller" method="get">
                <input type="hidden" name="command" value="customer_music_page"/>
                <input type="hidden" name="user_id" value="${sessionScope.user.id}"/>
                <button class="top-button">
                    <fmt:message key="my_music"/>
                </button>
            </form>
            <div class="ordered_songs">
                <c:choose>
                    <c:when test="${fn:length(sessionScope.buffered_id_songs) gt 0}">
                        ${fn:length(sessionScope.buffered_id_songs)}
                    </c:when>
                    <c:otherwise>
                        0
                    </c:otherwise>
                </c:choose>
            </div>
            <form action="Controller" method="get">
                <input type="hidden" name="command" value="customer_order_page"/>
                <button class="top-button">
                    <fmt:message key="cart"/>
                </button>
            </form>
            <form action="Controller" method="get">
                <input type="hidden" name="command" value="search_page"/>
                <button class="top-button">
                    <fmt:message key="search"/>
                </button>
            </form>
        </div>
    </div>
</fmt:bundle>
