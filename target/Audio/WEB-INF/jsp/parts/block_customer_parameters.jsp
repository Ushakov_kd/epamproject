<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<c:choose>
    <c:when test="${not empty requestScope.customer.discount}">
        <c:set var="discount" value="${requestScope.customer.discount}"/>
    </c:when>
    <c:otherwise>
        <c:set var="discount" value="0"/>
    </c:otherwise>
</c:choose>
<fmt:bundle basename="local">
<form action="Controller" method="get" class="form_customer">
    <input type="hidden" name="command" value="change_customer"/>
    <input type="hidden" name="user_id" value="${requestScope.customer.id}"/>
    <div class="user_field_name">
        <fmt:message key="user.login"/>
    </div>
    <div class="user_field_value">
        ${requestScope.customer.login}
    </div>
    <div class="user_field_name">
        <fmt:message key="user.discount"/>
    </div>
    <input type="text" name="discount" class="input_discount"
           value="${discount}" pattern="[0-9]{1,2}"/>
    <div class="user_field_name">
        <fmt:message key="user.block"/>
    </div>
    <select class="select_user_block" name="block">
        <c:set var="block" value="${requestScope.customer.block}"/>

        <c:choose>
            <c:when test="${not requestScope.customer.block}">
                <option selected value="0"><fmt:message key="block.false"/></option>
                <option value="1"><fmt:message key="block.true"/></option>
            </c:when>
            <c:otherwise>
                <option selected value="1"><fmt:message key="block.true"/></option>
                <option value="0"><fmt:message key="block.false"/></option>
            </c:otherwise>
        </c:choose>
    </select>
    <button class="button_change_user"><fmt:message key="change"/></button>
</form>
</fmt:bundle>