<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="local">
    <html>
    <head>
        <title><fmt:message key="title"/></title>
        <link href="css/top.css" rel="stylesheet" type="text/css">
        <link href="css/search.css" rel="stylesheet" type="text/css">
        <script src="js/jquery.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <jsp:include page="/WEB-INF/jsp/parts/top_admin.jsp"/>
    <body>
    <p class="search_word">
        <fmt:message key="button.create"/>
    </p>
    <form action="Controller" method="get" class="search_form">
        <select class="search_type" name="command" id="search_select">
            <option selected value="create_song"><fmt:message key="search.song"/></option>
            <option value="create_album"><fmt:message key="search.album"/></option>
            <option value="create_collection"><fmt:message key="search.collection"/></option>
        </select>
        <select class="search_type" name="genre">
            <c:forEach var="genre" items="${sessionScope.genres}">
                <option value="${genre}"><fmt:message key="genre.${genre}"/></option>
            </c:forEach>
        </select>
        <input type="text" name="artist" class="search_input" id="input_artist"
               placeholder="<fmt:message key="placeholder.artist"/>" pattern="[\.0-9A-Za-z'!- ]{2,20}"/>
        <input type="text" name="title" class="search_input"
               placeholder="<fmt:message key="placeholder.title"/>" pattern="[\.0-9A-Za-z'!- ]{1,20}"/>
        <input type="text" name="price" class="search_input" id="input_price"
               placeholder="<fmt:message key="placeholder.price"/>" pattern="[0-9]{1,2}(\.[0-9]{1,2})?"/>
        <button class="search-button"><fmt:message key="button.create"/></button>
    </form>
    <c:if test="${not empty requestScope.message}">
        <p class="error_message"><fmt:message key="${requestScope.message}"/></p>
    </c:if>
    <script>
        $('#search_select').change(function () {
            if($(this).val() == 'create_collection'){
                $('#input_artist').prop("disabled", true);
                $('#input_price').prop("disabled", true);
            }
            if($(this).val() == 'create_album'){
                $('#input_price').prop("disabled", true);
                $('#input_artist').prop("disabled", false);
            }
            if($(this).val() == 'create_song'){
                $('#input_price').prop("disabled", false);
                $('#input_artist').prop("disabled", false);
            }
        });
    </script>
    </body>
    </html>
</fmt:bundle>