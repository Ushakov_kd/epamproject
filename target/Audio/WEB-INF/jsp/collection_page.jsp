<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<ctg:headerPath name="top" role="${sessionScope.user.role}"/>
<fmt:bundle basename="local">
    <c:if test="${collection['class'].simpleName == 'Album'}">
        <c:set var="delete" value="delete_album_song"/>
    </c:if>
    <c:if test="${collection['class'].simpleName == 'MusicCollection'}">
        <c:set var="delete" value="delete_collection_song"/>
    </c:if>
    <c:choose>
        <c:when test="${not empty songs}">
            <c:set var="list" value="${songs}"/>
            <c:set var="element_page" value="song_page"/>
            <c:set var="id" value="song_id"/>
            <fmt:message var="element_symbol" key="symbol.song"/>
        </c:when>
        <c:otherwise>
            <c:set var="list" value="${collections}"/>
            <c:set var="id" value="collection_id"/>
            <fmt:message var="element_symbol" key="symbol.album"/>
            <ctg:accessAdmin>
                <c:set var="element_page" value="admin_collection_page"/>
            </ctg:accessAdmin>
            <ctg:accessCustomer>
                <c:set var="element_page" value="collection_page"/>
            </ctg:accessCustomer>
        </c:otherwise>
    </c:choose>
    <ctg:paginationInit capacity="6" list="${list}"/>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link href="css/top.css" rel="stylesheet" type="text/css">
        <link href="css/customer.css" rel="stylesheet" type="text/css">
        <link href="css/order_block.css" rel="stylesheet" type="text/css">
        <link href="css/customer_block.css" rel="stylesheet" type="text/css">
        <link href="css/admin.css" rel="stylesheet" type="text/css">
        <link href="css/autocomplete.css" rel="stylesheet" type="text/css">
        <link href="css/jquery-ui.css" rel="stylesheet">
        <script src="js/jquery.min.js" type="text/javascript"></script>
        <script src="js/jquery-1.10.2.js"></script>
        <script src="js/jquery-ui.js"></script>
        <title>iAudio</title>
    </head>
    <jsp:include page="${top}"/>
    <body>
    <div class="base">
        <div class="songs-block">
            <div class="songs_title">
                <c:choose>
                    <c:when test="${not empty collection}">
                        <c:if test="${collection['class'].simpleName == 'Album'}">
                            ${collection.artist} -
                        </c:if>
                        ${collection.title}
                    </c:when>
                    <c:otherwise><fmt:message key="all_songs_for_you"/></c:otherwise>
                </c:choose>
            </div>
            <div class="songs">
                <div class="left_options_column" id="check_box_column">
                    <ctg:accessAdmin>
                        <c:if test="${not empty collection}">
                            <c:forEach var="element" items="${songs}" begin="${startElement}"
                                       end="${endElement}">
                                <form action="Controller" method="get">
                                    <input type="hidden" name="command" value="${delete}"/>
                                    <input type="hidden" name="song_id" value="${element.id}"/>
                                    <input type="hidden" name="collection_id" value="${collection.id}"/>
                                    <button class="garbage_button">
                                        <fmt:message key="symbol.garbage"/>
                                    </button>
                                </form>
                            </c:forEach>
                        </c:if>
                    </ctg:accessAdmin>
                    <c:if test="${not empty songs}">
                        <c:if test="${sessionScope.buffered_id_songs != null}">
                            <form action="Controller" method="get" id="checked_boxes">
                                <input type="hidden" name="command" value="order"/>
                                <input type="hidden" name="previousQuery" value="${pageContext.request.queryString}"/>
                                <input type="hidden" name="pageIterator" value="${pageIterator}"/>
                                <input type="hidden" name="user_id" value="${user.id}"/>
                                <c:forEach var="element" items="${songs}"
                                           begin="${startElement}" end="${endElement}">
                                    <input type="checkbox" class="check_box" name="song_id" value="${element.id}"/>
                                </c:forEach>
                            </form>
                        </c:if>
                    </c:if>
                </div>
                <div class="line_stack">
                    <c:forEach var="element" items="${list}" begin="${startElement}" end="${endElement}">
                        <div class="info_line">
                            <form action="Controller" method="get">
                                <input type="hidden" name="command" value="${element_page}"/>
                                <input type="hidden" name="${id}" value="${element.id}"/>
                                <input type="hidden" name="previousQuery" value="${pageContext.request.queryString}"/>
                                <button class="element_button">
                                    <c:if test="${element['class'].simpleName != 'MusicCollection'}">
                                        <input type="hidden" name="type" value="album"/>
                                        ${element.artist} -
                                    </c:if>
                                        ${element.title}
                                </button>
                                <div class="element_info"><p>${element_symbol}</p></div>
                            </form>
                        </div>
                    </c:forEach>
                    <ctg:accessAdmin>
                        <c:if test="${not empty collection}">
                            <form action="Controller" method="get" class="add_song_form">
                                <input type="hidden" name="command" value="add_song_to_collection"/>
                                <input type="text" id="song" name="song_name" placeholder="enter_title"/>
                                <c:if test="${collection['class'].simpleName != 'MusicCollection'}">
                                    <input type="hidden" name="type" value="album"/>
                                </c:if>
                                <input type="hidden" name="collection_id" value="${collection.id}"/>
                                <input type="hidden" name="previousQuery" value="${pageContext.request.queryString}"/>
                                <button class="add_song_button">
                                    +
                                </button>
                            </form>
                        </c:if>
                    </ctg:accessAdmin>
                    <c:if test="${empty list}">
                        <p class="error_message"><fmt:message key="no_songs"/></p>
                    </c:if>
                </div>
                <ctg:paginationButtons capacity="${capacity}" currentPage="${pageIterator}"
                                       quantity="${entitiesNumber}"/>
            </div>
            <c:if test="${not empty requestScope.message}">
                <p class="error_message"><fmt:message key="${requestScope.message}"/></p>
            </c:if>
        </div>
        <ctg:accessAdmin>
            <c:if test="${not empty collection}">
                <jsp:include page="/WEB-INF/jsp/parts/block_collection_parameters.jsp"/>
            </c:if>
        </ctg:accessAdmin>
        <ctg:accessCustomer>
            <jsp:include page="/WEB-INF/jsp/parts/block_add_songs.jsp"/>
        </ctg:accessCustomer>
    </div>
    <script>
        $('#check_box_column input:checkbox').click(function () {
            $('#result span').html($('#check_box_column input:checkbox:checked').length);
        });
    </script>
    <script>
        $(function () {
            var availableTags = [];

            <c:forEach var="song" items="${requestScope.allSongs}">
            availableTags.push("${song}");
            </c:forEach>

            $("#song").autocomplete({
                max: 10,
                source: availableTags
            });
        });
    </script>
    <script>
        $(function() {
            $('.button_change_user').click(function() {
                return window.confirm(<fmt:message key="are_you_sure"/>);
            });
        });
    </script>
    </body>
    </html>
</fmt:bundle>