<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="local">
    <ctg:headerPath name="top" role="${sessionScope.user.role}"/>
    <html>
    <head>
        <title><fmt:message key="search"/></title>
        <link href="css/top.css" rel="stylesheet" type="text/css">
        <link href="css/search.css" rel="stylesheet" type="text/css">
        <script src="js/jquery.min.js" type="text/javascript"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <jsp:include page="${top}"/>
    <body>
    <p class="search_word">
        <fmt:message key="search"/>
    </p>
    <form action="Controller" method="get" class="search_form">
        <select class="search_type" name="command" id="search_select">
            <option selected value="search_song"><fmt:message key="search.song"/></option>
            <option value="search_album"><fmt:message key="search.album"/></option>
            <option value="search_collection"><fmt:message key="search.collection"/></option>
        </select>
        <select class="search_type" name="genre">
            <c:forEach var="genre" items="${sessionScope.genres}">
                <option value="${genre}"><fmt:message key="genre.${genre}"/></option>
            </c:forEach>
        </select>
        <input type="text" name="artist" class="search_input" id="input_artist"
               placeholder="<fmt:message key="placeholder.artist"/>" pattern="[0-9A-Za-z'!- \.]{,20}"/>
        <input type="text" name="title" class="search_input"
               placeholder="<fmt:message key="placeholder.title"/>" pattern="[0-9A-Za-z'!- \.]{,20}"/>

        <button class="search-button"><fmt:message key="search"/></button>
    </form>
    <c:if test="${not empty requestScope.message}">
        <p class="error_message"><fmt:message key="${requestScope.message}"/></p>
    </c:if>
    <script>
        $('#search_select').change(function () {
            if ($(this).val() == 'search_collection') {
                $('#input_artist').prop("disabled", true);
            } else {
                $('#input_artist').prop("disabled", false);
            }
        });
    </script>
    </body>
    </html>
</fmt:bundle>