<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="local">
    <c:set var="list" value="${requestScope.users}"/>
    <fmt:message var="element_symbol" key="symbol.user"/>
    <ctg:paginationInit capacity="6" list="${list}"/>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link href="css/top.css" rel="stylesheet" type="text/css">
        <link href="css/admin.css" rel="stylesheet" type="text/css">
        <link href="css/customer.css" rel="stylesheet" type="text/css">
        <link href="css/customer_block.css" rel="stylesheet" type="text/css">
        <script src="js/jquery.min.js" type="text/javascript"></script>
        <title>iAudio</title>
    </head>
    <jsp:include page="/WEB-INF/jsp/parts/top_admin.jsp"/>
    <body>
    <div class="base">
        <div class="songs-block">
            <div class="songs_title">
                <fmt:message key="customers"/>
            </div>
            <div class="songs">
                <div class="line_stack">
                    <div class="customer_line">
                        <div class="login_table"><fmt:message key="user.login"/></div>
                        <div class="discount_table"><fmt:message key="user.discount"/></div>
                        <div class="block_table"><fmt:message key="user.block"/></div>
                    </div>
                    <c:forEach var="element" items="${list}" begin="${startElement}" end="${endElement}">
                        <div class="customer">
                            <form action="Controller" method="get">
                                <input type="hidden" name="command" value="show_customer"/>
                                <input type="hidden" name="user_id" value="${element.id}"/>
                                <button class="element_button">
                                    <div class="login">${element.login}</div>
                                    <div class="discount">${element.discount}%</div>
                                    <div class="block"><fmt:message key="block.${element.block}"/></div>
                                </button>
                            </form>
                        </div>
                    </c:forEach>
                </div>
                <ctg:paginationButtons capacity="${capacity}" currentPage="${pageIterator}"
                                       quantity="${entitiesNumber}"/>
            </div>
            <c:if test="${not empty requestScope.message}">
                <p class="error_message"><fmt:message key="${requestScope.message}"/></p>
            </c:if>
        </div>
        <c:if test="${not empty requestScope.customer}">
            <jsp:include page="/WEB-INF/jsp/parts/block_customer_parameters.jsp"/>
        </c:if>
    </div>
    <script>
        $(function () {
            $('.button_change_user').click(function () {
                return window.confirm("<fmt:message key="are_you_sure"/>");
            });
        });
    </script>
    </body>
    </html>
</fmt:bundle>