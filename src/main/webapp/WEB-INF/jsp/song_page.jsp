<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<ctg:paginationInit capacity="6" list="${requestScope.comments}"/>
<fmt:bundle basename="local">
    <ctg:headerPath name="top" role="${sessionScope.user.role}"/>
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link href="css/top.css" rel="stylesheet" type="text/css">
        <link href="css/customer.css" rel="stylesheet" type="text/css">
        <link href="css/info_block.css" rel="stylesheet" type="text/css">
        <link href="css/comment_block.css" rel="stylesheet" type="text/css">
        <link href="css/customer_block.css" rel="stylesheet" type="text/css">
        <link href="css/admin.css" rel="stylesheet" type="text/css">
        <script src="js/jquery.min.js" type="text/javascript"></script>
        <title><fmt:message key="comments"/></title>
    </head>
    <jsp:include page="${top}"/>
    <body>
    <div class="base">
        <div class="songs-block">
            <div class="songs_title">
                <fmt:message key="comments"/>
            </div>
            <div class="songs">
                <div class="line_stack">
                    <form action="Controller" method="get">
                        <c:forEach var="comment" items="${comments}" begin="${startElement}"
                                   end="${endElement}">
                            <div class="info_line">
                                    ${comment.userName}: ${comment.text}
                            </div>
                        </c:forEach>
                        <c:if test="${empty comments}">
                            <p class="error_message"><fmt:message key="no_comments"/></p>
                        </c:if>
                    </form>
                </div>
                <ctg:paginationButtons capacity="${capacity}" currentPage="${pageIterator}"
                                       quantity="${entitiesNumber}"/>
            </div>
            <ctg:accessCustomer>
                <div class="comment-block">
                    <form action="Controller" method="get">
                        <input type="hidden" name="command" value="add_comment"/>
                        <input type="hidden" name="song_id" value="${song.id}"/>
                        <input type="hidden" name="user_id" value="${user.id}"/>
                        <input type="hidden" name="page" value="${pageContext.request.servletPath}"/>
                        <textarea class="comment-textarea" name="text" placeholder="<fmt:message key="enter_comment"/>"></textarea>
                        <button class="comment-button">
                            <fmt:message key="add"/>
                        </button>
                    </form>
                </div>
            </ctg:accessCustomer>
            <c:if test="${not empty requestScope.message}">
                <p class="error_message"><fmt:message key="${requestScope.message}"/></p>
            </c:if>
        </div>
        <ctg:accessCustomer>
            <jsp:include page="/WEB-INF/jsp/parts/block_song_info.jsp"/>
        </ctg:accessCustomer>
        <ctg:accessAdmin>
            <jsp:include page="/WEB-INF/jsp/parts/block_song_parameters.jsp"/>
        </ctg:accessAdmin>
    </div>
    <script>
        $(function () {
            $('.button_change_user').click(function () {
                return window.confirm("<fmt:message key="are_you_sure"/>");
            });
        });
    </script>
    </body>
    </html>
</fmt:bundle>