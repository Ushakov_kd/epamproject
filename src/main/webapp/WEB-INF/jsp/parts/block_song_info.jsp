<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="local">
<div class="info-block">
    <div class="song_info">
        <div class="info_name">
            <fmt:message key="artist"/>
        </div>
        <div class="info_value">
            ${song.artist}
        </div>
        <div class="info_name">
            <fmt:message key="title"/>
        </div>
        <div class="info_value">
            ${song.title}
        </div>
        <div class="info_name">
            <fmt:message key="genre"/>
        </div>
        <div class="info_value">
            <fmt:message key="genre.${song.genre}"/>
        </div>
        <div class="info_name">
            $  <fmt:message key="price"/>
        </div>
        <div class="info_value">
            ${song.price}
        </div>
    </div>
</div>
</fmt:bundle>