<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="local">
<form action="Controller" method="get" class="form_customer">
    <input type="hidden" name="command" value="change_collection_parameters"/>
    <input type="hidden" name="element_id" value="${collection.id}"/>
    <div class="song_info">
        <c:if test="${collection['class'].simpleName == 'Album'}">
            <div class="user_field_name">
                <fmt:message key="artist"/>
            </div>
            <div class="user_field_value">
                    ${collection.artist}
            </div>
        </c:if>
        <div class="user_field_name">
            <fmt:message key="title"/>
        </div>
        <div class="user_field_value">
            ${collection.title}
        </div>
        <div class="user_field_name">
            <fmt:message key="genre"/>
        </div>
        <div class="user_field_value">
            <fmt:message key="genre.${collection.genre}"/>
        </div>
    </div>
</form>
</fmt:bundle>