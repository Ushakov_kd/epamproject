<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<c:if test="${collection['class'].simpleName == 'Album'}">
    <c:set var="change" value="change_album_parameters"/>
    <c:set var="element_id" value="album_id"/>
</c:if>
<c:if test="${collection['class'].simpleName == 'MusicCollection'}">
    <c:set var="change" value="change_collection_parameters"/>
    <c:set var="element_id" value="collection_id"/>
</c:if>
<fmt:bundle basename="local">
<form action="Controller" method="get" class="form_customer">
    <input type="hidden" name="command" value="${change}"/>
    <input type="hidden" name="${element_id}" value="${collection.id}"/>
    <div class="song_info">
        <c:if test="${collection['class'].simpleName == 'Album'}">
            <div class="user_field_name">
                <fmt:message key="artist"/>
            </div>
            <input type="text" name="artist" class="input_discount" required="true"
                   value="${collection.artist}" pattern="[0-9A-Za-z\.' ]{2,20}"/>
        </c:if>
        <div class="user_field_name">
            <fmt:message key="title"/>
        </div>
        <input type="text" name="title" class="input_discount" required="true"
               value="${collection.title}" pattern="[0-9A-Za-z\.' ]{2,20}"/>
        <div class="user_field_name">
            <fmt:message key="genre"/>
        </div>
        <select class="select_genre" name="genre">
            <option class="option_genre" selected value="${collection.genre}">
                <fmt:message key="genre.${collection.genre}"/>
            </option>
            <c:forEach var="item" items="${genres}">
                <c:if test="${collection.genre != item}">
                    <option class="option_genre" value="${item}"><fmt:message key="genre.${item}"/></option>
                </c:if>
            </c:forEach>
        </select>
    </div>
    <button class="button_change_user"> <fmt:message key="change"/></button>
</form>
</fmt:bundle>