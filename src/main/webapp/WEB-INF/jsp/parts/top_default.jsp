<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="local">
    <div class="top">
        <div class="button_space">
            <form action="Controller" method="get">
                <input type="hidden" name="command" value="start_page"/>
                <button class="main-page-button">iAudio</button>
            </form>
            <div class="dropdown">
                <button class="drop-button">
                    <fmt:message key="language"/>
                </button>
                <div class="dropdown-content">
                    <form action="Controller" method="get" class="locale_form">
                        <input type="hidden" name="command" value="set_locale"/>
                        <input type="hidden" name="locale" value="en"/>
                        <input type="hidden" name="previousQuery" value="command=login_page"/>
                        <button class="locale_button">En</button>
                    </form>
                    <form action="Controller" method="get" class="locale_form">
                        <input type="hidden" name="command" value="set_locale"/>
                        <input type="hidden" name="locale" value="ru"/>
                        <input type="hidden" name="previousQuery" value="command=login_page"/>
                        <button class="locale_button">Ru</button>
                    </form>
                </div>
            </div>
            <form action="Controller" method="get">
                <input type="hidden" name="command" value="login_page"/>
                <button class="top-button">
                    <fmt:message key="login"/>
                </button>
            </form>
        </div>
    </div>
</fmt:bundle>
