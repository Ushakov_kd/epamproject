<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="local">
<form action="Controller" method="get" class="form_customer">
    <input type="hidden" name="command" value="change_song_parameters"/>
    <input type="hidden" name="song_id" value="${song.id}"/>
    <div class="song_info">
        <div class="user_field_name">
            <fmt:message key="artist"/>
        </div>
        <input type="text" name="artist" class="input_discount" required="true"
               value="${song.artist}" pattern="[0-9A-Za-z\.' ]{2,20}"/>
        <div class="user_field_name">
            <fmt:message key="title"/>
        </div>
        <input type="text" name="title" class="input_discount" required="true"
               value="${song.title}" pattern="[0-9A-Za-z\.' ]{2,20}"/>
        <div class="user_field_name">
            <fmt:message key="genre"/>
        </div>
        <select class="select_genre" name="genre">
            <option class="option_genre" selected value="${song.genre}">
                <fmt:message key="genre.${song.genre}"/>
            </option>
            <c:forEach var="item" items="${genres}">
                <c:if test="${song.genre != item}">
                    <option class="option_genre" value="${item}"><fmt:message key="genre.${item}"/>
                    </option>
                </c:if>
            </c:forEach>
        </select>
        <div class="user_field_name">
           $ <fmt:message key="price"/>
        </div>
        <input type="text" name="price" class="input_discount" required="true"
               value="${song.price}" pattern="[0-9]{1,2}(\.[0-9]{1,2})?"/>
    </div>
    <button class="button_change_user"><fmt:message key="change"/></button>
</form>
</fmt:bundle>