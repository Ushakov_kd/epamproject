<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="local">
    <ctg:headerPath name="top" role="${sessionScope.user.role}"/>
    <html>
    <head>
        <title><fmt:message key="error"/></title>
        <link href="css/top.css" rel="stylesheet" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <jsp:include page="${top}"/>
    <body>
    <p align="center" style="font-weight: bold; color:#FF0000; font-size:30px"><fmt:message key="error"/></p>
    <c:if test="${not empty requestScope.message}">
        <p align="center" style="font-style: italic"><fmt:message key="${requestScope.message}"/></p>
    </c:if>
    </body>
    </html>
</fmt:bundle>