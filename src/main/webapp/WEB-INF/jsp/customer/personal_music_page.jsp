<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<ctg:paginationInit capacity="6" list="${requestScope.user_songs}"/>
<fmt:bundle basename="local">
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link href="css/top.css" rel="stylesheet" type="text/css">
        <link href="css/customer.css" rel="stylesheet" type="text/css">
        <script src="js/jquery.min.js"></script>
        <title><fmt:message key="my_music"/></title>
    </head>
    <jsp:include page="/WEB-INF/jsp/parts/top_customer.jsp"/>
    <body>
    <div class="base">
        <div class="songs-block">
            <div class="songs_title">
                <fmt:message key="my_music"/>
            </div>
            <div class="songs">
                <div class="left_options_column">
                    <c:forEach var="element" items="${requestScope.user_songs}" begin="${startElement}"
                               end="${endElement}">
                        <form action="Controller" method="get">
                            <input type="hidden" name="command" value="delete_user_song"/>
                            <input type="hidden" name="song_id" value="${element.id}"/>
                            <input type="hidden" name="user_id" value="${user.id}"/>
                            <button class="garbage_button">
                                <fmt:message key="symbol.garbage"/>
                            </button>
                        </form>
                    </c:forEach>
                </div>
                <div class="line_stack">
                    <form action="Controller" method="get">
                        <c:forEach var="element" items="${requestScope.user_songs}" begin="${startElement}"
                                   end="${endElement}">
                            <div class="info_line">
                                <input type="hidden" name="command" value="song_page"/>
                                <input type="hidden" name="song_id" value="${element.id}"/>
                                <button class="element_button">
                                        ${element.artist} - ${element.title}
                                </button>
                            </div>
                        </c:forEach>
                    </form>
                    <c:if test="${empty requestScope.user_songs}">
                        <p class="error_message"><fmt:message key="user_has_not_songs"/></p>
                    </c:if>
                </div>
                <ctg:paginationButtons capacity="${capacity}" currentPage="${pageIterator}"
                                       quantity="${entitiesNumber}"/>
            </div>
        </div>
        <script>
            $(function () {
                $('.garbage_button').click(function () {
                    return window.confirm(<fmt:message key="are_you_sure"/>);
                });
            });
        </script>
    </div>
    </body>
    </html>
</fmt:bundle>