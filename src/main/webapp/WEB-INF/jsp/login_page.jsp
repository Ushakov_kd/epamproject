<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="local">
    <fmt:message key="placeholder.login" var="login"/>
    <fmt:message key="placeholder.password" var="password"/>
    <fmt:message key="sign_in" var="sign_in"/>
    <html>
    <head>
        <title> <fmt:message key="enter"/></title>
        <link href="css/top.css" rel="stylesheet" type="text/css">
        <link href="css/login.css" rel="stylesheet" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <ctg:headerPath name="top"/>
    <jsp:include page="${top}"/>
    <body>
    <p class="enter">
        <fmt:message key="enter"/>
    </p>
    <form action="Controller" method="post">
        <input type="hidden" name="command" value="login"/>
        <input type="text" name="login" class="input" value=""
               placeholder="${login}" required pattern="[0-9A-Za-z]{3,20}"/>
        <input type="password" name="password" class="input" value=""
               placeholder="${password}" required pattern="[0-9A-Za-z]{4,20}"/>
        <button class="login_button">${sign_in}</button>
    </form>

    <c:if test="${not empty requestScope.message}">
        <p class="error_message"><fmt:message key="${requestScope.message}"/></p>
    </c:if>
    </body>
    </html>
</fmt:bundle>
