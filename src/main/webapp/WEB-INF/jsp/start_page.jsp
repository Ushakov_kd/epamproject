<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="local">
    <html>
    <head>
        <title>iAudio</title>
        <link href="css/top.css" rel="stylesheet" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <ctg:headerPath name="top"/>
    <jsp:include page="${top}"/>
    <body>
    <p align="center" style="font-weight: bold; color:#FF0000; font-size:30px"><fmt:message key="welcome"/></p>
    </body>
    </html>
</fmt:bundle>