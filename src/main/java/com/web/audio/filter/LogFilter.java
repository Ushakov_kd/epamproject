package com.web.audio.filter;

import com.web.audio.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LogFilter implements Filter {

    private static final Logger LOGGER = LogManager.getLogger(LogFilter.class);

    private static final String USER_ATTRIBUTE = "user";
    private static final String UNAUTHORIZED_USER = "UNAUTHORIZED: ";
    private static final String NO_SESSION = "NO SESSION: ";

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) request;

        StringBuilder stringBuilder = new StringBuilder();

        HttpSession session = httpRequest.getSession(false);
        if(session != null) {
            stringBuilder.append("SessionID='" + session.getId() + "' ");
            User user = (User) session.getAttribute(USER_ATTRIBUTE);
            if (user != null) {
                stringBuilder.append(user.getRole() + "ID='" + user.getId() + "' LOGIN='" + user.getLogin() + "' -> ");
            }else {
                stringBuilder.append(UNAUTHORIZED_USER);
            }
        } else {
            stringBuilder.append(NO_SESSION + UNAUTHORIZED_USER);
        }
        addRequestInfo(stringBuilder, httpRequest);

        LOGGER.debug(stringBuilder.toString());
        chain.doFilter(request,response);
    }

    private void addRequestInfo(StringBuilder stringBuilder, HttpServletRequest httpRequest) {
        List<String> list = new ArrayList<>();
        list.add(httpRequest.getScheme()+"://");
        list.add(httpRequest.getServerName()+":");
        list.add(String.valueOf(httpRequest.getServerPort()));
        list.add(httpRequest.getContextPath());
        list.add(httpRequest.getServletPath()+"?");
        list.add(httpRequest.getPathInfo());
        list.add(httpRequest.getQueryString());

        for(String element :list) {
            if (element != null) {
                stringBuilder.append(element);
            }
        }
    }
}
