package com.web.audio.filter;

import com.web.audio.controller.command.resource.PageManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class SessionFilter implements Filter {

    private static final Logger LOGGER = LogManager.getLogger(SessionFilter.class);
    private static final String ERROR_MESSAGE = "Session check failed! Redirect!";
    private static final String SUCCESSFUL_CHECK_MESSAGE = "Session check is successful! ";

    private static final String LOGIN_PAGE_QUERY = "command=login_page";
    private static final String INDEX_PAGE_PATH = "/index.jsp";


    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpSession session = httpRequest.getSession(false);

        if (session != null) {
            chain.doFilter(request, response);
            return;
        }
        String query = httpRequest.getQueryString();
        String servletPath = httpRequest.getServletPath();
        if (INDEX_PAGE_PATH.equals(servletPath) || LOGIN_PAGE_QUERY.equals(query)) {
            LOGGER.debug(SUCCESSFUL_CHECK_MESSAGE + servletPath + "?" + query);
            chain.doFilter(request, response);
        } else {
            LOGGER.debug(ERROR_MESSAGE);
            HttpServletResponse httpResponse = (HttpServletResponse) response;
            httpResponse.sendRedirect(PageManager.REDIRECT_LOGIN_PAGE_COMMAND);
        }
    }
}
