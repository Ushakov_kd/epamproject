package com.web.audio.filter;

import com.web.audio.controller.command.resource.*;
import com.web.audio.entity.Role;
import com.web.audio.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class AccessFilter implements Filter {

    private static final Logger LOGGER = LogManager.getLogger(AccessFilter.class);
    private static final String ACCESS_ERROR_MESSAGE = "No access! ";
    private static final String UNEXPECTED_ROLE_ERROR_MESSAGE = "Unexpected role! ";
    private static final String ERROR_MESSAGE_ATTRIBUTE = "errorMessage";

    private static final String USER_ATTRIBUTE = "user";

    private static final String COMMAND_PARAMETER = "command";


    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        String requestCommand = request.getParameter(COMMAND_PARAMETER);
        if (requestCommand == null) {
            chain.doFilter(request, response);
            return;
        }

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpSession session = httpRequest.getSession(false);

        List<String> permittedCommands = createPermittedCommands(session);

        if(isValidCommand(requestCommand, permittedCommands)){
            chain.doFilter(request, response);
        } else {
            LOGGER.debug(ACCESS_ERROR_MESSAGE);
            request.setAttribute(ERROR_MESSAGE_ATTRIBUTE, ACCESS_ERROR_MESSAGE);
            RequestDispatcher dispatcher = request.getRequestDispatcher(PageManager.ERROR_PAGE_PATH);
            dispatcher.forward(request, response);
        }
    }

    private boolean isValidCommand(String requestCommand, List<String> permittedCommands){
        for (String permittedCommand : permittedCommands) {
            if (permittedCommand.equals(requestCommand)) {
                return true;
            }
        }
        return false;
    }

    private List<String> createPermittedCommands(HttpSession session) {
        if (session == null) {
            return CommandNameUnauthorized.getNamesAsList();
        }
        User user = (User) session.getAttribute(USER_ATTRIBUTE);
        if (user == null) {
            return CommandNameUnauthorized.getNamesAsList();
        }
        Role role = user.getRole();
        List<String> permittedCommands = CommandNameAuthorized.getNamesAsList();
        switch (role) {
            case CUSTOMER:
                permittedCommands.addAll(CommandNameCustomer.getNamesAsList());
                return permittedCommands;
            case ADMIN:
                permittedCommands.addAll(CommandNameAdmin.getNamesAsList());
                return permittedCommands;
            default:
                throw new IllegalArgumentException(UNEXPECTED_ROLE_ERROR_MESSAGE);
        }
    }


}
