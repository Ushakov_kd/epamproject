package com.web.audio.entity;

import java.util.Objects;

public class User implements Identifiable {

    private Role role;
    private int id;
    private String login;
    private int discount;
    private boolean block;


    public User() {
    }

    public String getLogin() {
        return login;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public boolean isBlock() {
        return block;
    }

    public void setBlock(boolean block) {
        this.block = block;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Role getRole() {
        return role;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || this.getClass() != object.getClass()) {
            return false;
        }
        User user = (User) object;
        if (!(this.id == user.id)) {
            return false;
        }
        if (!this.login.equals(user.login)) {
            return false;
        }
        if (!(this.role == user.role)) {
            return false;
        }
        if (!(this.discount == user.discount)) {
            return false;
        }
        if (!(this.block == user.block)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return (int) 31 * id + login.hashCode() + role.hashCode() +
                discount + Objects.hashCode(block);
    }

    @Override
    public String toString() {
        return "User{role='" + role + "', login='" + login + "', id='"
                + id + "', discount='" + discount + "', block='" + block + "'}";
    }
}
