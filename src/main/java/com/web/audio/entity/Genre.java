package com.web.audio.entity;

public enum Genre {
    CLASSICAL, ELECTRONIC, JAZZ, POP, RAP, ROCK, REGGAE;
}
