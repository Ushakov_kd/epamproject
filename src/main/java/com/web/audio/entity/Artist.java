package com.web.audio.entity;

import java.util.Objects;

public class Artist implements Identifiable {

    private int id;
    private String name;

    public Artist() {
    }

    public Artist(int id, String name) {
        this.id = id;
        this.name = name;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Artist{" + "id=" + id + ", name='" + name + "'}";
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        Artist artist = (Artist) object;
        if (this.id != artist.id) {
            return false;
        }
        if (this.name != artist.name) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return 31 * id + name.hashCode();
    }
}
