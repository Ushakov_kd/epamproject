package com.web.audio.entity;

import java.math.BigDecimal;
import java.util.Objects;

public class Song implements Identifiable {

    private int id;
    private String artist;
    private String title;
    private Genre genre;
    private BigDecimal price;

    public Song() {
    }

    public Song(String artist, String title, Genre genre, BigDecimal price) {
        this.artist = artist;
        this.title = title;
        this.genre = genre;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || this.getClass() != object.getClass()) {
            return false;
        }
        Song song = (Song) object;
        if(!(id == song.id)){
            return false;
        }
        if(!artist.equals(song.artist)){
            return false;
        }
        if(!title.equals(song.title)){
            return false;
        }
        if(!genre.equals(song.genre)){
            return false;
        }
        if(!price.equals(song.price)){
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return (int) 31*id + artist.hashCode()+title.hashCode()+genre.hashCode()+price.hashCode();
    }

    @Override
    public String toString() {
        return "Song{" + "id='" + id + "', artist='" + artist + "', title='" + title +
                "', genre='" + genre + "', price='" + price +'}';
    }
}
