package com.web.audio.entity.builder;

import com.web.audio.entity.Genre;
import com.web.audio.entity.Song;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SongBuilder implements EntityBuilder {

    private static final int ID_INDEX = 1;
    private static final int ARTIST_INDEX = 2;
    private static final int TITLE_INDEX = 3;
    private static final int PRICE_INDEX = 4;
    private static final int GENRE_INDEX = 5;

    @Override
    public Song build (ResultSet resultSet) throws SQLException {
        Song song = new Song();

        song.setId(resultSet.getInt(ID_INDEX));
        song.setArtist(resultSet.getString(ARTIST_INDEX));
        song.setTitle(resultSet.getString(TITLE_INDEX));
        String genre = resultSet.getString(GENRE_INDEX);
        song.setGenre(Genre.valueOf(genre));

        String dbPrice = resultSet.getString(PRICE_INDEX);
        song.setPrice(new BigDecimal(dbPrice));

        return song;
    }
}
