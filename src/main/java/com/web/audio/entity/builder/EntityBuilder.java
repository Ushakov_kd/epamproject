package com.web.audio.entity.builder;

import com.web.audio.entity.Identifiable;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface EntityBuilder <T extends Identifiable>{

    /**
     * Gets ResultSet for create object defined by class
     * as generic. ResultSet is coming from database
     * at sql query.
     *
     * @param resultSet required to set entity fields.
     * @return a new object <code>T extend Identifable</code>
     * @exception SQLException if a ResultSet do not have
     * specific field which you called.
     */
    T build(ResultSet resultSet) throws SQLException;

}
