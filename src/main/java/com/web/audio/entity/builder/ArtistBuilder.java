package com.web.audio.entity.builder;

import com.web.audio.entity.Artist;
import com.web.audio.entity.Identifiable;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ArtistBuilder implements EntityBuilder {

    private static final int ID_INDEX = 1;
    private static final int NAME_INDEX = 2;

    @Override
    public Identifiable build(ResultSet resultSet) throws SQLException {
        Artist artist = new Artist();

        artist.setId(resultSet.getInt(ID_INDEX));
        artist.setName(resultSet.getString(NAME_INDEX));

        return artist;
    }
}
