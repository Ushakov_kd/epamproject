package com.web.audio.entity.builder;

import com.web.audio.entity.Comment;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CommentBuilder implements EntityBuilder {

    private static final int ID_INDEX = 1;
    private static final int USER_INDEX = 2;
    private static final int TEXT_INDEX = 3;

    @Override
    public Comment build (ResultSet resultSet) throws SQLException {
        Comment comment = new Comment();

        comment.setId(resultSet.getInt(ID_INDEX));
        comment.setUserName(resultSet.getString(USER_INDEX));
        comment.setText(resultSet.getString(TEXT_INDEX));

        return comment;
    }
}
