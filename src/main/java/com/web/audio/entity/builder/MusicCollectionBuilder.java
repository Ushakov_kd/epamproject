package com.web.audio.entity.builder;

import com.web.audio.entity.Genre;
import com.web.audio.entity.MusicCollection;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MusicCollectionBuilder implements EntityBuilder {

    private static final int ID_INDEX = 1;
    private static final int TITLE_INDEX = 2;
    private static final int GENRE_INDEX = 3;

    @Override
    public MusicCollection build (ResultSet resultSet) throws SQLException {
        MusicCollection collection = new MusicCollection();

        collection.setId(resultSet.getInt(ID_INDEX));
        collection.setTitle(resultSet.getString(TITLE_INDEX));
        String genre = resultSet.getString(GENRE_INDEX);
        collection.setGenre(Genre.valueOf(genre));
        return collection;
    }

}
