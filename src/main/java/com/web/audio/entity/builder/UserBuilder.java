package com.web.audio.entity.builder;

import com.web.audio.entity.Role;
import com.web.audio.entity.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserBuilder implements EntityBuilder{

    private static final int ID_INDEX = 1;
    private static final int LOGIN_INDEX = 2;
    private static final int ROLE_INDEX = 3;
    private static final int DISCOUNT_INDEX = 4;
    private static final int BLOCK_INDEX = 5;

    @Override
    public User build(ResultSet resultSet) throws SQLException {
        User user = new User();

        user.setId(resultSet.getInt(ID_INDEX));
        user.setLogin(resultSet.getString(LOGIN_INDEX));
        String role = resultSet.getString(ROLE_INDEX);
        user.setRole(Role.valueOf(role));
        user.setDiscount(resultSet.getInt(DISCOUNT_INDEX));
        user.setBlock(resultSet.getBoolean(BLOCK_INDEX));

        return user;
    }
}
