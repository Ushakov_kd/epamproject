package com.web.audio.entity.builder;

import com.web.audio.entity.Album;
import com.web.audio.entity.Genre;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AlbumBuilder implements EntityBuilder {

    private static final int ID_INDEX = 1;
    private static final int ARTIST_INDEX = 2;
    private static final int TITLE_INDEX = 3;
    private static final int GENRE_INDEX = 4;

    @Override
    public Album build (ResultSet resultSet) throws SQLException {
        Album album = new Album();

        album.setId(resultSet.getInt(ID_INDEX));
        album.setArtist(resultSet.getString(ARTIST_INDEX));
        album.setTitle(resultSet.getString(TITLE_INDEX));
        String genre = resultSet.getString(GENRE_INDEX);
        album.setGenre(Genre.valueOf(genre));
        return album;
    }

}
