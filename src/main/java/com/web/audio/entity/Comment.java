package com.web.audio.entity;

public class Comment implements Identifiable {

    private int id;
    private String userName;
    private String text;

    public Comment() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        Comment comment = (Comment) object;
        if (!(id == comment.id)) {
            return false;
        }
        if (!userName.equals(comment.userName)) {
            return false;
        }
        if (!text.equals(comment.text)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return (int) 31 * id + userName.hashCode() + text.hashCode();
    }

    @Override
    public String toString() {
        return "Song{" + "id='" + id + "', userName='" + userName + "', text='" + text + "'}";
    }

}
