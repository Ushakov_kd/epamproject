package com.web.audio.entity;


public class Album implements Identifiable{

    private int id;
    private String artist;
    private String title;
    private Genre genre;

    public Album() {
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || this.getClass() != object.getClass()) {
            return false;
        }
        Album album = (Album) object;

        if (!(id == album.id)) {
            return false;
        }
        if (!this.artist.equals(album.artist)) {
            return false;
        }
        if (!this.title.equals(album.title)) {
            return false;
        }
        if (!this.genre.equals(album.genre)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return 31 * id + title.hashCode() + genre.hashCode()
                + artist.hashCode();
    }

    @Override
    public String toString() {
        return "Album{" + "id='" + id + "', artist='" + artist + "', title='" + title +
                "', genre='" + genre + "'}";
    }
}
