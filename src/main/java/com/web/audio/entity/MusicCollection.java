package com.web.audio.entity;

public class MusicCollection implements Identifiable {

    private int id;
    private String title;
    private Genre genre;

    public MusicCollection() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || this.getClass() != object.getClass()) {
            return false;
        }
        MusicCollection collection = (MusicCollection) object;

        if (!(id == collection.id)) {
            return false;
        }
        if (!this.title.equals(collection.title)) {
            return false;
        }
        if (!this.genre.equals(collection.genre)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return (int) 31 * id + title.hashCode() + genre.hashCode();
    }

    @Override
    public String toString() {
        return "MusicCollection{" + "id='" + id + "', title='" + title +
                "', genre='" + genre + "'}";
    }
}
