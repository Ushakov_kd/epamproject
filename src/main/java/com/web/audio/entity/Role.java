package com.web.audio.entity;

public enum Role {

    ADMIN, CUSTOMER;
}
