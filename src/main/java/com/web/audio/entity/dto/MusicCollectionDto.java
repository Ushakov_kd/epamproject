package com.web.audio.entity.dto;

import com.web.audio.entity.Genre;

public class MusicCollectionDto {
    private String collectionId;
    private String title;
    private Genre genre;

    public MusicCollectionDto() {
    }

    public MusicCollectionDto(String collectionId, String title, Genre genre) {
        this.collectionId = collectionId;
        this.title = title;
        this.genre = genre;
    }

    public MusicCollectionDto(String title, Genre genre) {
        this.title = title;
        this.genre = genre;
    }

    public String getCollectionId() {
        return collectionId;
    }

    public void setCollectionId(String collectionId) {
        this.collectionId = collectionId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }
}
