package com.web.audio.entity.dto;

import com.web.audio.entity.Genre;

import java.math.BigDecimal;

public class SongDto {
    private String songId;
    private String artist;
    private String title;
    private Genre genre;
    private BigDecimal price;

    public SongDto(String songId, String artist, String title, Genre genre, BigDecimal price) {
        this.songId = songId;
        this.artist = artist;
        this.title = title;
        this.genre = genre;
        this.price = price;
    }

    public SongDto(String artist, String title, Genre genre, BigDecimal price) {
        this.artist = artist;
        this.title = title;
        this.genre = genre;
        this.price = price;
    }
    public SongDto(String artist, String title) {
        this.artist = artist;
        this.title = title;
    }


    public SongDto(){}

    public String getSongId() {
        return songId;
    }

    public void setSongId(String songId) {
        this.songId = songId;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
