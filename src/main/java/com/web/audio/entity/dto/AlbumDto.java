package com.web.audio.entity.dto;

import com.web.audio.entity.Genre;

public class AlbumDto {

    private String albumId;
    private String artist;
    private String title;
    private Genre genre;

    public AlbumDto (){}

    public AlbumDto (String albumId, String artist, String title, Genre genre){
        this.albumId = albumId;
        this.artist = artist;
        this.title = title;
        this.genre = genre;
    }


    public AlbumDto (String artist, String title, Genre genre){
        this.artist = artist;
        this.title = title;
        this.genre = genre;
    }

    public String getAlbumId() {
        return albumId;
    }

    public void setAlbumId(String albumId) {
        this.albumId = albumId;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }
}
