package com.web.audio.service;

import com.web.audio.dao.exception.DaoException;
import com.web.audio.dao.impl.UserDao;
import com.web.audio.entity.User;
import com.web.audio.service.exception.ServiceException;

import java.util.List;
import java.util.Optional;

public class UserService {

    private UserDao userDao;

    public UserService (UserDao userDao) {
        this.userDao = userDao;
    }

    public Optional<User> login(String login, String password) throws ServiceException {
        try {
            Optional<User> user = userDao.findUserByLoginAndPassword(login, password);
            return user;
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public List<User> getAllCustomers() throws ServiceException{
        try{
            List<User> customers = userDao.getAllCustomers();
            return customers;
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public Optional<User> findUserById (String userId) throws ServiceException {
        try {
            Optional<User> user = userDao.findUserById(userId);
            return user;
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public void changeUserDiscount(String customerId, String discount) throws ServiceException {
        try {
           userDao.updateUserDiscount(customerId, discount);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public void changeUserBlock(String customerId, String block) throws ServiceException {
        try {
            userDao.updateUserBlock(customerId, block);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

}
