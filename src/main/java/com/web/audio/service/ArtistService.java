package com.web.audio.service;

import com.web.audio.dao.exception.DaoException;
import com.web.audio.dao.impl.ArtistDao;
import com.web.audio.entity.Artist;
import com.web.audio.service.exception.ServiceException;

import java.util.Optional;

public class ArtistService {

    private ArtistDao artistDao;

    public ArtistService(ArtistDao artistDao) {
        this.artistDao = artistDao;
    }

    public int getArtistId(String artistName) throws ServiceException {
        if (!doesArtistExist(artistName)) {
            createArtist(artistName);
        }
        try {
            Optional<Artist> optionalArtist = artistDao.findArtistByName(artistName);
            Artist artist = optionalArtist.get();
            return artist.getId();
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    private void createArtist(String name) throws ServiceException {
        try {
            artistDao.createArtist(name);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    private boolean doesArtistExist(String name) throws ServiceException {
        try {
            Optional<Artist> artist = artistDao.findArtistByName(name);
            if (artist.isPresent()) {
                return true;
            } else {
                return false;
            }
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
}
