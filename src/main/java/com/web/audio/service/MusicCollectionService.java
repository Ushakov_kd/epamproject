package com.web.audio.service;

import com.web.audio.dao.exception.DaoException;
import com.web.audio.dao.impl.MusicCollectionDao;
import com.web.audio.entity.MusicCollection;
import com.web.audio.entity.dto.MusicCollectionDto;
import com.web.audio.service.exception.ServiceException;

import java.util.List;
import java.util.Optional;

public class MusicCollectionService {


    private MusicCollectionDao musicCollectionDao;

    public MusicCollectionService(MusicCollectionDao musicCollectionDao) {
        this.musicCollectionDao = musicCollectionDao;
    }

    public void changeAllCollectionParameters(MusicCollectionDto collectionDto) throws ServiceException{
        try {
            musicCollectionDao.updateAllCollectionColumnsByCollectionId(collectionDto);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public Optional<MusicCollection> findCollectionById(String id) throws ServiceException {
        try {
            return musicCollectionDao.findCollectionById(id);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public Optional<MusicCollection> findSingleCollectionByDto(MusicCollectionDto collectionDto) throws ServiceException {
        try {
            return musicCollectionDao.findSingleMusicCollectionByTitleAndGenre(collectionDto);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public List<MusicCollection> findCollectionsByVariableNumberParameters(MusicCollectionDto collectionDto) throws ServiceException {
        try {
            List<MusicCollection> collections =  musicCollectionDao.findMusicCollectionsByVariableNumberParameters(collectionDto);
            return collections;
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }


    public void createCollection(MusicCollectionDto collectionDto) throws ServiceException {
        try {
            musicCollectionDao.addCollection(collectionDto);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

}
