package com.web.audio.service;

import com.web.audio.dao.DaoFactory;
import com.web.audio.dao.impl.*;

public class ServiceFactory {

    private DaoFactory daoFactory;

    public ServiceFactory(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    public UserService getUserService() {
        UserDao userDao = daoFactory.getUserDao();
        return new UserService(userDao);
    }

    public SongService getSongService() {
        SongDao songDao = daoFactory.getSongDao();
        return new SongService(songDao);
    }

    public MusicCollectionService getMusicCollectionService() {
        MusicCollectionDao musicCollectionDao = daoFactory.getMusicCollectionDao();
        return new MusicCollectionService(musicCollectionDao);
    }

    public AlbumService getAlbumService() {
        AlbumDao albumDao = daoFactory.getAlbumDao();
        return new AlbumService(albumDao);
    }

    public CommentService getCommentService() {
        CommentDao commentDao = daoFactory.getCommentDao();
        return new CommentService(commentDao);
    }

    public ArtistService getArtistService() {
        ArtistDao artistDao = daoFactory.getArtistDao();
        return new ArtistService(artistDao);
    }

}
