package com.web.audio.service;

import com.web.audio.dao.exception.DaoException;
import com.web.audio.dao.impl.CommentDao;
import com.web.audio.entity.Comment;
import com.web.audio.service.exception.ServiceException;

import java.util.List;

public class CommentService {

    private CommentDao commentDao;

    public CommentService(CommentDao commentDao) {
        this.commentDao = commentDao;
    }

    public List<Comment> findSongCommentsBySongId(String songId) throws ServiceException {
        try {
            return commentDao.findSongCommentsBySongId(songId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public void addComment(String songId, String userId, String text) throws ServiceException {
        try{
            commentDao.addComment(songId, userId, text);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }


}
