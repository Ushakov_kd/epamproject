package com.web.audio.service;

import com.web.audio.dao.exception.DaoException;
import com.web.audio.dao.impl.SongDao;
import com.web.audio.entity.Song;
import com.web.audio.entity.dto.SongDto;
import com.web.audio.service.exception.ServiceException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SongService {

    private static final String SPLITTER = " - ";

    private SongDao songDao;

    public SongService(SongDao songDao) {
        this.songDao = songDao;
    }

    public void createSong(SongDto songDto) throws ServiceException {
        try {
           songDao.createSong(songDto);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public void addSongToCollection(String collectionId, String songId) throws ServiceException {
        try{
            songDao.addSongToCollection(collectionId, songId);
        } catch (DaoException e){
            throw new ServiceException(e);
        }
    }

    public void addPaidSongsToUser(String userId, List<String> songsId) throws ServiceException {
        try {
            songDao.addSongsToUser(String.valueOf(userId), songsId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public void deleteSongFromCollection(String collectionId, String songId) throws ServiceException {
        try {
            songDao.deleteSongFromCollectionByCollectionIdAndSongId(collectionId, songId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
    public void deleteSongFromUserSongs(String userId, String songId) throws ServiceException {
        try {
            songDao.deleteUserSong(String.valueOf(userId), String.valueOf(songId));
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public void changeAllSongParameters(SongDto dto) throws ServiceException {
        try {
            songDao.updateAllSongColumnsBySongId(dto);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
    public List<Song> findCollectionSongs(String collectionId) throws ServiceException {
        try {
           return songDao.findSongsByCollectionId(collectionId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public List<Song> findSongsByVariableNumberParameters(SongDto songDto) throws ServiceException {
        try {
            return songDao.findSongsByVariableNumberParameters(songDto);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public Optional<Song> findSingleSongByDto(SongDto songDto) throws ServiceException {
        try {
            return songDao.findSingleSongByVariableNumberParameters(songDto);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public List<Song> getAllSongs() throws ServiceException {
        try {
            return songDao.getAllSongs();
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public List<String> getAllSongsNames() throws ServiceException {
            List<Song> allSongs = getAllSongs();
            List<String> names = new ArrayList<>();
            for(Song song : allSongs){
                String name = song.getArtist()+SPLITTER+song.getTitle();
                names.add(name);
            }
            return names;

    }

    public Optional<Song> findSongById(String songId) throws ServiceException {
        try {
            return songDao.findSongById(songId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public List<Song> findSongsById(List<String> idSongsList) throws ServiceException {
        List<Song> songs = new ArrayList<>();
        for (String songId : idSongsList) {
            try {
                Optional<Song> song = songDao.findSongById(songId);
                song.ifPresent(songs::add);
            } catch (DaoException e) {
                throw new ServiceException(e);
            }
        }
        return songs;
    }

    public boolean hasCustomerTheSong(String userId, String songId) throws ServiceException {
        try {
            Optional<Song> song = songDao.findUserSongByUserIdSongId(userId, songId);
            return song.isPresent();
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
    public boolean hasCollectionTheSong(String collectionId, String songId) throws ServiceException {
        try {
            Optional<Song> song = songDao.findCollectionSongByCollectionIdSongId(collectionId, songId);
            return song.isPresent();
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }



    public List<Song> findUserSongsByUserId(String userId) throws ServiceException {
        try {
            return songDao.findUserSongsByUserId(userId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public BigDecimal calculateTotalPrice(List<Song> songs) {
        List<Song> orderedSongs = new ArrayList<>();
        for (Song song : songs) {
            orderedSongs.add(song);
        }
        return orderedSongs.stream().map(Song::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

}
