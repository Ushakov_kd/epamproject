package com.web.audio.service;

import com.web.audio.dao.exception.DaoException;
import com.web.audio.dao.impl.AlbumDao;
import com.web.audio.entity.Album;
import com.web.audio.entity.dto.AlbumDto;
import com.web.audio.service.exception.ServiceException;

import java.util.List;
import java.util.Optional;

public class AlbumService {

    private AlbumDao albumDao;

    public AlbumService(AlbumDao albumDao) {
        this.albumDao = albumDao;
    }

    public void changeAllAlbumParameters(AlbumDto dto) throws ServiceException {
        try {
            albumDao.updateAllAlbumColumnsByAlbumId(dto);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public Optional<Album> findAlbumById(String albumId) throws ServiceException {
        try {
            return albumDao.findAlbumById(albumId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public Optional<Album> findSingleAlbumByDto(AlbumDto albumDto) throws ServiceException {
        try {
            return albumDao.findSingleAlbumByArtistNameTitleGenre(albumDto);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public List<Album> findAlbumsByVariableNumberParameters(AlbumDto albumDto) throws ServiceException {
        try {
            return albumDao.findAlbumsByVariableNumberParameters(albumDto);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    public void createAlbum(AlbumDto albumDto) throws ServiceException {
        try {
            albumDao.addAlbum(albumDto);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
}
