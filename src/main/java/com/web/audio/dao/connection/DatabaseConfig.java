package com.web.audio.dao.connection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class DatabaseConfig {

    private static final Logger LOGGER = LogManager.getLogger(DatabaseConfig.class);
    private static final String DB_PROPERTIES_FILE_NAME = "db.properties";
    private Properties properties;

    private static final String DB_POOL_SIZE = "db.pool_size";
    private static final String DB_DRIVER = "db.driver";
    private static final String DB_USER = "db.user";
    private static final String DB_PASSWORD = "db.password";
    private static final String DB_URL = "db.url";


    public DatabaseConfig() {
        init();
    }

    public String getDriver() {
        return properties.getProperty(DB_DRIVER);
    }

    public String getUser() {
        return properties.getProperty(DB_USER);
    }

    public String getPassword() {
        return properties.getProperty(DB_PASSWORD);
    }

    public String getUrl() {
        return properties.getProperty(DB_URL);
    }

    public int getPoolSize() {
        String poolSize = properties.getProperty(DB_POOL_SIZE);
        return Integer.parseInt(poolSize);
    }

    private void init() {
        try {
            properties = new Properties();
            ClassLoader loader = getClass().getClassLoader();
            InputStream stream = loader.getResourceAsStream(DB_PROPERTIES_FILE_NAME);
            properties.load(stream);
        } catch (IOException e) {
            LOGGER.warn(e.getMessage());
            throw new IllegalStateException(e);
        }
    }
}
