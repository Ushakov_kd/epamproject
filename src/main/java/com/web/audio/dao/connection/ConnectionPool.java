package com.web.audio.dao.connection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;

public class ConnectionPool {

    private static final Logger LOGGER = LogManager.getLogger(ConnectionPool.class);

    private static final Semaphore SEMAPHORE = new Semaphore(5, true);
    private static ReentrantLock POOL_LOCK = new ReentrantLock();
    private static ReentrantLock CONNECTION_LOCK = new ReentrantLock();
    private static AtomicReference<ConnectionPool> INSTANCE_REFERENCE = new AtomicReference<>();

    private List<ProxyConnection> freeConnections = new LinkedList<>();
    private List<ProxyConnection> busyConnections = new LinkedList<>();

    public static ConnectionPool getInstance() {
        if (INSTANCE_REFERENCE.get() == null) {
            POOL_LOCK.lock();
            try {
                if (INSTANCE_REFERENCE.get() == null) {
                    final ConnectionPool pool = new ConnectionPool();
                    pool.initPool();
                    INSTANCE_REFERENCE.set(pool);
                }
            } finally {
                POOL_LOCK.unlock();
            }
        }
        return INSTANCE_REFERENCE.get();
    }

    public ProxyConnection getConnection() {
        CONNECTION_LOCK.lock();
        try {
            SEMAPHORE.acquire();
            ProxyConnection connection = freeConnections.remove(0);
            busyConnections.add(connection);
            return connection;
        } catch (InterruptedException e) {
            LOGGER.warn(e);
        } finally {
            CONNECTION_LOCK.unlock();
        }
        return null;
    }

    public void putConnection(ProxyConnection connection) {
        CONNECTION_LOCK.lock();
        try {
            if (busyConnections.remove(connection)) {
                freeConnections.add(connection);
                SEMAPHORE.release();
            }
        } finally {
            CONNECTION_LOCK.unlock();
        }
    }

    private void initPool() {
        DatabaseConfig manager = new DatabaseConfig();

        int poolSize = manager.getPoolSize();
        String driver = manager.getDriver();
        String user = manager.getUser();
        String password = manager.getPassword();
        String url = manager.getUrl();

        for (int i = 0; i < poolSize; i++) {
            try {
                Class.forName(driver);
                Connection connection = DriverManager.getConnection(url, user, password);
                ProxyConnection proxyConnection = new ProxyConnection(connection);
                freeConnections.add(proxyConnection);
            } catch (ClassNotFoundException | SQLException e) {
                LOGGER.warn(e.getMessage(), e);
            }
        }
    }
}