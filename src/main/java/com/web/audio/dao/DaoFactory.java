package com.web.audio.dao;

import com.web.audio.dao.connection.ProxyConnection;
import com.web.audio.dao.impl.*;
import com.web.audio.entity.builder.*;

public class DaoFactory {

    private ProxyConnection connection;

    public DaoFactory(ProxyConnection connection) {
        this.connection = connection;
    }

    public UserDao getUserDao() {
        return new UserDao(connection, new UserBuilder());
    }

    public SongDao getSongDao() {
        return new SongDao(connection, new SongBuilder());
    }

    public MusicCollectionDao getMusicCollectionDao() {
        return new MusicCollectionDao(connection, new MusicCollectionBuilder());
    }

    public AlbumDao getAlbumDao() {
        return new AlbumDao(connection, new AlbumBuilder());
    }

    public CommentDao getCommentDao() {
        return new CommentDao(connection, new CommentBuilder());
    }

    public ArtistDao getArtistDao(){
        return new ArtistDao(connection, new ArtistBuilder());
    }
}
