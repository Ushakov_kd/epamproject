package com.web.audio.dao.impl;

import com.web.audio.dao.AbstractDao;
import com.web.audio.dao.connection.ProxyConnection;
import com.web.audio.dao.exception.DaoException;
import com.web.audio.entity.Album;
import com.web.audio.entity.Genre;
import com.web.audio.entity.builder.EntityBuilder;
import com.web.audio.entity.dto.AlbumDto;

import java.util.List;
import java.util.Optional;

public class AlbumDao extends AbstractDao {

    private static final String SELECT_ALBUM_BY_ID =  "SELECT collection_id, artist_name, title, genre " +
            "FROM collection NATURAL JOIN artist WHERE type='album' AND collection_id = ?";
    private static final String INSERT_ALBUM_INTO_COLLECTIONS_QUERY =
            "INSERT INTO collection (type, artist_id, title, genre) VALUE ('album', ?, ?, ?)";
    private static final String SELECT_ALBUMS_BY_PARAMETERS_RAW_QUERY =
            "SELECT collection_id, artist_name, title, genre " +
                    "FROM collection NATURAL JOIN artist WHERE type='album'";
    private static final String UPDATE_COLLECTION_QUERY =
            "UPDATE collection SET artist_id=?, title=?, genre=? WHERE collection_id=?";

    public AlbumDao(ProxyConnection connection, EntityBuilder builder) {
        super(connection, builder);
    }


    public List<Album> findAlbumsByVariableNumberParameters(AlbumDto album) throws DaoException {
        String query = prepareQuery(album);
        return executeQuery(query);
    }

    public void addAlbum(AlbumDto albumDto) throws DaoException {
        String artist = albumDto.getArtist();
        String title = albumDto.getTitle();
        Genre genre = albumDto.getGenre();
        execute(INSERT_ALBUM_INTO_COLLECTIONS_QUERY, artist, title, genre.name());
    }

    public Optional<Album> findAlbumById(String id) throws DaoException {
        return executeQueryForSingleResult(SELECT_ALBUM_BY_ID, id);
    }

    public Optional<Album> findSingleAlbumByArtistNameTitleGenre(AlbumDto album) throws DaoException {
        String query = prepareQuery(album);
        return executeQueryForSingleResult(query);
    }

    public void updateAllAlbumColumnsByAlbumId(AlbumDto albumDto) throws DaoException {
        String id = albumDto.getAlbumId();
        String artist = albumDto.getArtist();
        String title = albumDto.getTitle();
        Genre genre = albumDto.getGenre();
        execute(UPDATE_COLLECTION_QUERY, artist, title, genre.name(), id);
    }

    private String prepareQuery(AlbumDto album){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(SELECT_ALBUMS_BY_PARAMETERS_RAW_QUERY);

        String artist = album.getArtist();
        String title = album.getTitle();
        Genre genre = album.getGenre();

        if (artist != null) {
            stringBuilder.append(" AND artist_name=\"" + artist + "\"");
        }
        if (title != null) {
            stringBuilder.append(" AND title=\"" + title + "\"");
        }
        if (genre != null) {
            stringBuilder.append(" AND genre=\"" + genre.name() + "\"");
        }
        stringBuilder.append(";");
        return stringBuilder.toString();
    }
}
