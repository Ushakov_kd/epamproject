package com.web.audio.dao.impl;

import com.web.audio.dao.AbstractDao;
import com.web.audio.dao.connection.ProxyConnection;
import com.web.audio.dao.exception.DaoException;
import com.web.audio.entity.Comment;
import com.web.audio.entity.builder.EntityBuilder;

import java.util.List;

public class CommentDao extends AbstractDao {

    private static final String SELECT_SONG_COMMENTS_BY_SONG_ID_QUERY =
            "SELECT comment.comment_id, user.login, comment.text FROM comment NATURAL JOIN user WHERE song_id=?";
    private static final String INSERT_COMMENT_QUERY
            = "INSERT INTO comment (song_id, user_id, text) VALUE (?, ?, ?)";


    public CommentDao(ProxyConnection connection, EntityBuilder builder) {
        super(connection, builder);
    }

    public List<Comment> findSongCommentsBySongId(String songId) throws DaoException {
        return executeQuery(SELECT_SONG_COMMENTS_BY_SONG_ID_QUERY, songId);
    }

    public void addComment(String songId, String userId, String text) throws DaoException {
        execute(INSERT_COMMENT_QUERY, songId, userId, text);
    }

}
