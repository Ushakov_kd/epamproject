package com.web.audio.dao.impl;

import com.web.audio.dao.AbstractDao;
import com.web.audio.dao.connection.ProxyConnection;
import com.web.audio.dao.exception.DaoException;
import com.web.audio.entity.User;
import com.web.audio.entity.builder.EntityBuilder;

import java.util.List;
import java.util.Optional;

public class UserDao extends AbstractDao {

    private static final String FIND_USER_BY_LOGIN_PASSWORD_QUERY
            = "SELECT user_id, login, role, discount, block FROM user WHERE login=? AND password=?";
    private static final String UPDATE_CUSTOMER_DISCOUNT
            = "UPDATE user SET discount = ? WHERE user_id = ?";
    private static final String UPDATE_CUSTOMER_BLOCK
            = "UPDATE user SET block = ? WHERE user_id = ?";
    private static final String ALL_CUSTOMERS_QUERY = "SELECT user_id, login, role, discount, block " +
            "FROM user WHERE role='customer'";
    private static final String FIND_USER_BY_ID_QUERY
            = "SELECT user_id, login, role, discount, block FROM user WHERE user_id=?";



    public UserDao(ProxyConnection connection, EntityBuilder builder) {
        super(connection, builder);
    }

    public Optional<User> findUserByLoginAndPassword(String login, String password) throws DaoException {
        return executeQueryForSingleResult(FIND_USER_BY_LOGIN_PASSWORD_QUERY, login, password);
    }

    public List<User> getAllCustomers() throws DaoException{
        return executeQuery(ALL_CUSTOMERS_QUERY);
    }

    public Optional<User> findUserById(String userId) throws DaoException {
        return executeQueryForSingleResult(FIND_USER_BY_ID_QUERY, userId);
    }

    public void updateUserDiscount(String userId, String discount) throws DaoException {
        execute(UPDATE_CUSTOMER_DISCOUNT, discount, userId);
    }

    public void updateUserBlock(String userId, String block) throws DaoException {
        execute(UPDATE_CUSTOMER_BLOCK, block, userId);
    }
}
