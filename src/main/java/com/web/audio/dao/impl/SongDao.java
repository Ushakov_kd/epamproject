package com.web.audio.dao.impl;

import com.web.audio.dao.AbstractDao;
import com.web.audio.dao.connection.ProxyConnection;
import com.web.audio.dao.exception.DaoException;
import com.web.audio.entity.Genre;
import com.web.audio.entity.Song;
import com.web.audio.entity.builder.EntityBuilder;
import com.web.audio.entity.dto.SongDto;

import java.util.List;
import java.util.Optional;

public class SongDao extends AbstractDao {
    private static final String SQL_OPERATOR_AND = " AND ";
    private static final String SQL_OPERATOR_WHERE = " WHERE ";
    private static final String DELETE_COLLECTION_SONG_QUERY
            = "DELETE FROM collection_songs WHERE collection_id = ? AND song_id = ?";
    private static final String INSERT_SONG_TO_COLLECTION_QUERY
            = "INSERT INTO collection_songs (collection_id, song_id) VALUE (?, ?)";
    private static final String INSERT_SONG_TO_SONG_QUERY
            = "INSERT INTO song (artist_id, title, genre, price) VALUE (?, ?, ?, ?)";
    private static final String DELETE_USER_SONG_QUERY
            = "DELETE FROM user_songs WHERE user_id=? AND song_id=?";
    private static final String INSERT_USER_SONGS_QUERY
            = "INSERT INTO user_songs (user_id, song_id) VALUE (?, ?)";
    private static final String SELECT_SONG_BY_ID =
            "SELECT song_id, artist_name, title, price, genre FROM song NATURAL JOIN artist WHERE song_id=?";
    private static final String COLLECTION_SONGS_BY_COLLECTION_ID_QUERY =
            "SELECT song_id, artist_name, title, price, genre FROM song NATURAL JOIN artist WHERE song_id IN " +
                    "(SELECT song_id FROM collection_songs WHERE collection_id=?)";
    private static final String SELECT_COLLECTION_SONG_BY_COLLECTION_ID_AND_SONG_ID =
            "SELECT song_id, artist_name, title, price, genre FROM song NATURAL JOIN artist WHERE song_id IN " +
                    "(SELECT song_id FROM collection_songs WHERE collection_id=? AND song_id=?)";
    private static final String SELECT_SONG_BY_SONG_ID_AND_USER_ID_QUERY =
            "SELECT song_id, artist_name, title, price, genre FROM song NATURAL JOIN artist WHERE song_id IN " +
                    "(SELECT song_id FROM user_songs WHERE user_id = ? AND song_id=?)";
    private static final String USER_SONGS_BY_USER_ID_QUERY =
            "SELECT song_id, artist_name, title, price, genre FROM song NATURAL JOIN artist WHERE song_id IN " +
                    "(SELECT song_id FROM user_songs WHERE user_id = ?)";
    private static final String ALL_SONGS_QUERY =
            "SELECT song_id, artist_name, title, price, genre FROM song NATURAL JOIN artist";
    private static final String SELECT_SONGS_BY_PARAMETERS_RAW_QUERY =
            "SELECT song_id, artist_name, title, price, genre FROM song NATURAL JOIN artist";
    private static final String UPDATE_SONG_QUERY =
            "UPDATE song SET artist_id=?, title=?, genre=?, price=? WHERE song_id=?";

    public SongDao(ProxyConnection connection, EntityBuilder builder) {
        super(connection, builder);
    }

    public void deleteSongFromCollectionByCollectionIdAndSongId(String collectionId, String songId) throws DaoException {
        execute(DELETE_COLLECTION_SONG_QUERY, collectionId, songId);
    }

    public void createSong(SongDto songDto) throws DaoException {
        String artistId = songDto.getArtist();
        String title = songDto.getTitle();
        Genre genre = songDto.getGenre();
        String price = songDto.getPrice().toString();
        execute(INSERT_SONG_TO_SONG_QUERY, artistId, title, genre.name(), price);
    }

    public void updateAllSongColumnsBySongId(SongDto songDto) throws DaoException {
        String songId = songDto.getSongId();
        String artistId = songDto.getArtist();
        String title = songDto.getTitle();
        Genre genre = songDto.getGenre();
        String price = songDto.getPrice().toString();
        execute(UPDATE_SONG_QUERY, artistId, title, genre.name(),price, songId);
    }

    public List<Song> getAllSongs() throws DaoException {
        return executeQuery(ALL_SONGS_QUERY);
    }

    public void deleteUserSong(String userId, String songId) throws DaoException {
        execute(DELETE_USER_SONG_QUERY, userId, songId);
    }

    public void addSongsToUser(String userId, List<String> songsId) throws DaoException {
        for (String songId : songsId) {
            execute(INSERT_USER_SONGS_QUERY, userId, songId);
        }
    }

    public void addSongToCollection(String collectionId, String songId) throws DaoException{
        execute(INSERT_SONG_TO_COLLECTION_QUERY, collectionId, songId);
    }

    public Optional<Song> findSingleSongByVariableNumberParameters(SongDto songDto) throws DaoException {
        String query = appEndSongParametersToQueryBySongDto(SELECT_SONGS_BY_PARAMETERS_RAW_QUERY, songDto);
        return executeQueryForSingleResult(query);
    }
    
    public List<Song> findSongsByVariableNumberParameters(SongDto songDto) throws DaoException {
        String query = appEndSongParametersToQueryBySongDto(SELECT_SONGS_BY_PARAMETERS_RAW_QUERY, songDto);
        return executeQuery(query);
    }

    public List<Song> findSongsByCollectionId(String collectionId) throws DaoException {
        return executeQuery(COLLECTION_SONGS_BY_COLLECTION_ID_QUERY, collectionId);
    }

    public Optional<Song> findUserSongByUserIdSongId(String userId, String songId) throws DaoException {
        return executeQueryForSingleResult(SELECT_SONG_BY_SONG_ID_AND_USER_ID_QUERY, userId, songId);
    }

    public Optional<Song> findCollectionSongByCollectionIdSongId(String collectionId, String songId) throws DaoException {
        return executeQueryForSingleResult(SELECT_COLLECTION_SONG_BY_COLLECTION_ID_AND_SONG_ID, collectionId, songId);
    }

    public List<Song> findUserSongsByUserId(String userId) throws DaoException {
        return executeQuery(USER_SONGS_BY_USER_ID_QUERY, userId);
    }

    public Optional<Song> findSongById(String songId) throws DaoException {
        return executeQueryForSingleResult(SELECT_SONG_BY_ID, songId);
    }
    private String appEndSongParametersToQueryBySongDto(String query, SongDto songDto) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(query);

        String artist = songDto.getArtist();
        String title = songDto.getTitle();
        Genre genre = songDto.getGenre();

        int parameterCounter = 0;
        if (artist != null) {
            String operator = getIntermediateQueryOperator(parameterCounter);
            stringBuilder.append(operator + " artist_name=\"" + artist + "\"");
            parameterCounter++;
        }
        if (title != null) {
            String operator = getIntermediateQueryOperator(parameterCounter);
            stringBuilder.append(operator +" title=\"" + title + "\"");
            parameterCounter++;
        }
        if (genre != null) {
            String operator = getIntermediateQueryOperator(parameterCounter);
            stringBuilder.append(operator +" genre=\"" + genre.name() + "\"");
            parameterCounter++;
        }
        stringBuilder.append(";");
        return stringBuilder.toString();
    }

    private String getIntermediateQueryOperator(int counter) {
        if(counter > 0) {
            return SQL_OPERATOR_AND;
        } else {
            return SQL_OPERATOR_WHERE;
        }
    }

}