package com.web.audio.dao.impl;

import com.web.audio.dao.AbstractDao;
import com.web.audio.dao.connection.ProxyConnection;
import com.web.audio.dao.exception.DaoException;
import com.web.audio.entity.Artist;
import com.web.audio.entity.builder.EntityBuilder;

import java.util.Optional;

public class ArtistDao extends AbstractDao {

    private static final String INSERT_ARTIST_QUERY = "INSERT INTO artist (artist_name) VALUE(?)";
    private static final String SELECT_ARTIST_BY_NAME_QUERY = "SELECT artist_id, artist_name FROM artist WHERE artist_name=?";


    public ArtistDao(ProxyConnection connection, EntityBuilder builder) {
        super(connection, builder);
    }

    public Optional<Artist> findArtistByName(String name) throws DaoException {
        return executeQueryForSingleResult(SELECT_ARTIST_BY_NAME_QUERY, name);
    }

    public void createArtist(String name) throws DaoException{
        execute(INSERT_ARTIST_QUERY, name);
    }

}
