package com.web.audio.dao.impl;

import com.web.audio.dao.AbstractDao;
import com.web.audio.dao.connection.ProxyConnection;
import com.web.audio.dao.exception.DaoException;
import com.web.audio.entity.Genre;
import com.web.audio.entity.MusicCollection;
import com.web.audio.entity.builder.EntityBuilder;
import com.web.audio.entity.dto.MusicCollectionDto;

import java.util.List;
import java.util.Optional;

public class MusicCollectionDao extends AbstractDao {

    private static final String SELECT_COLLECTION_BY_ID =  "SELECT collection_id, title, genre " +
            "FROM collection WHERE type='collection' AND collection_id = ?";
    private static final String INSERT_COLLECTION_INTO_COLLECTIONS_QUERY =
            "INSERT INTO collection (type, title, genre) VALUE ('collection', ?, ?)";
    private static final String SELECT_COLLECTIONS_BY_PARAMETERS_RAW_QUERY =
            "SELECT collection_id, title, genre " +
                    "FROM collection WHERE type='collection'";
    private static final String UPDATE_COLLECTION_QUERY =
            "UPDATE collection SET title=?, genre=? WHERE collection_id=?";

    public MusicCollectionDao(ProxyConnection connection, EntityBuilder builder) {
        super(connection, builder);
    }

    public Optional<MusicCollection> findSingleMusicCollectionByTitleAndGenre(MusicCollectionDto collectionDto) throws DaoException {
        String query = prepareQuery(collectionDto);
        return executeQueryForSingleResult(query);
    }

    public List<MusicCollection> findMusicCollectionsByVariableNumberParameters(MusicCollectionDto collectionDto) throws DaoException {
        String query = prepareQuery(collectionDto);
        return executeQuery(query);
    }

    public void updateAllCollectionColumnsByCollectionId(MusicCollectionDto collectionDto) throws DaoException {
        String collectionId = collectionDto.getCollectionId();
        String title = collectionDto.getTitle();
        Genre genre = collectionDto.getGenre();
        execute(UPDATE_COLLECTION_QUERY, title, genre.name(), collectionId);
    }

    public void addCollection(MusicCollectionDto collectionDto) throws DaoException {
        String title = collectionDto.getTitle();
        Genre genre = collectionDto.getGenre();
        execute(INSERT_COLLECTION_INTO_COLLECTIONS_QUERY, title, genre.name());
    }
    public Optional<MusicCollection> findCollectionById(String id) throws DaoException {
        return executeQueryForSingleResult(SELECT_COLLECTION_BY_ID, id);
    }

    private String prepareQuery(MusicCollectionDto collectionDto){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(SELECT_COLLECTIONS_BY_PARAMETERS_RAW_QUERY);

        String title = collectionDto.getTitle();
        Genre genre = collectionDto.getGenre();

        if (title != null) {
            stringBuilder.append(" AND title=\"" + title + "\"");
        }
        if (genre != null) {
            stringBuilder.append(" AND genre=\"" + genre.name() + "\"");
        }
        stringBuilder.append(";");
        return stringBuilder.toString();
    }

}
