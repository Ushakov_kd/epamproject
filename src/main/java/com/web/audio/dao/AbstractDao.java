package com.web.audio.dao;

import com.web.audio.dao.connection.ProxyConnection;
import com.web.audio.dao.exception.DaoException;
import com.web.audio.entity.Identifiable;
import com.web.audio.entity.builder.EntityBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class AbstractDao <T extends Identifiable> {

    private static final Logger LOGGER = LogManager.getLogger(AbstractDao.class);
    private static final String SQL_EXCEPTION = "SQL Exception by execute query";
    private static final String NOT_SINGLE_RESULT_EXCEPTION = "Query gave not single result";

    private ProxyConnection connection;
    private EntityBuilder builder;

    public AbstractDao(ProxyConnection connection, EntityBuilder<T> builder) {
        this.connection = connection;
        this.builder = builder;
    }

    protected void execute(String query, String... parameters) throws DaoException {
        try {
            PreparedStatement statement = connection.prepareStatement(query);
            setPreparedStatement(statement, parameters);
            LOGGER.debug(statement.toString());
            statement.execute();
        } catch (SQLException e) {
            throw new DaoException(SQL_EXCEPTION, e);
        }
    }

    protected List<T> executeQuery(String query, String... parameters) throws DaoException {
        try {
            PreparedStatement statement = connection.prepareStatement(query);
            setPreparedStatement(statement, parameters);

            LOGGER.debug(statement.toString());

            ResultSet resultSet = statement.executeQuery();
            List<T> entities = new ArrayList<>();
            while (resultSet.next()) {
                T entity = (T) builder.build(resultSet);
                entities.add(entity);
            }
            return entities;
        } catch (SQLException e) {
            throw new DaoException(SQL_EXCEPTION, e);
        }
    }

    private void setPreparedStatement(PreparedStatement statement, String... parameters) throws SQLException {
        for (int i = 0; i < parameters.length; i++) {
            statement.setString(i + 1, parameters[i]);
        }
    }


    protected Optional<T> executeQueryForSingleResult(String query, String... parameters) throws DaoException {

        List<T> entities = executeQuery(query, parameters);

        if (entities.size() == 1) {
            return Optional.of(entities.get(0));
        } else if (entities.size() > 1) {
            throw new DaoException(NOT_SINGLE_RESULT_EXCEPTION);
        }
        return Optional.empty();
    }
}
