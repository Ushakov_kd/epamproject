package com.web.audio.tag;

import com.web.audio.entity.Role;
import com.web.audio.entity.User;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.tagext.BodyTagSupport;

public class AccessCustomerTag extends BodyTagSupport {

    private static final String USER_ATTRIBUTE = "user";

    @Override
    public int doStartTag() {

        HttpSession session = pageContext.getSession();
        User user = (User) session.getAttribute(USER_ATTRIBUTE);

        Role role = user.getRole();
        switch (role) {
            case CUSTOMER:
                return EVAL_BODY_INCLUDE;
            default:
                return SKIP_BODY;
        }
    }
}
