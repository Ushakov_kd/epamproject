package com.web.audio.tag;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

public class PaginationButtonsTag extends TagSupport {

    private int currentPage;
    private int capacity;
    private int quantity;

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public int doStartTag() throws JspTagException {

        int numberPages = quantity / capacity;

        int rest = quantity % capacity;
        if (rest != 0) {
            numberPages++;
        }
        try {
            JspWriter out = pageContext.getOut();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("<div class='page_iterator_block'>");
            for (int i = 0; i < numberPages; i++) {
                int pageNumber = i + 1;
                if ((pageNumber) != currentPage) {
                    stringBuilder.append("<form action='Controller' method='get' class='page_iterator_button_form'>");
                    stringBuilder.append("<input type='hidden' name='command' value='set_pagination_iterator'/>");
                    stringBuilder.append("<input type='hidden' name='pageIterator' value='" + pageNumber + "'/>");
                    stringBuilder.append("<input type='hidden' name='previousQuery' value='"+((HttpServletRequest) pageContext.getRequest()).getQueryString()+"'/>");
                    stringBuilder.append("<button class='page_iterator_button'>" + pageNumber + "</button>");
                    stringBuilder.append("</form>");
                } else {
                    stringBuilder.append("<div class='page_iterator'>"+pageNumber+"</div>");
                }
            }
            stringBuilder.append("</div>");
            out.write(stringBuilder.toString());
        } catch (IOException e) {
            throw new JspTagException(e.getMessage());
        }
        return SKIP_BODY;
    }
}
