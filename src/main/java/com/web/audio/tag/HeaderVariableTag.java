package com.web.audio.tag;

import com.web.audio.entity.Role;
import javax.servlet.jsp.tagext.TagSupport;

public class HeaderVariableTag extends TagSupport {

    private Role role;
    private String name;

    public void setRole(Role role) {
        this.role = role;
    }

    public void setName(String name) {
        this.name = name;
    }

    private static final String ADMIN_TOP_JSP_PATH = "/WEB-INF/jsp/parts/top_admin.jsp";
    private static final String CUSTOMER_TOP_JSP_PATH = "/WEB-INF/jsp/parts/top_customer.jsp";
    private static final String DEFAULT_TOP_JSP_PATH = "/WEB-INF/jsp/parts/top_default.jsp";

    @Override
    public int doStartTag() {

        if(role == null) {
            pageContext.setAttribute(name, DEFAULT_TOP_JSP_PATH);
            return SKIP_BODY;
        }
        switch (role){
            case ADMIN:
                pageContext.setAttribute(name, ADMIN_TOP_JSP_PATH);
                return SKIP_BODY;
            case CUSTOMER:
                pageContext.setAttribute(name, CUSTOMER_TOP_JSP_PATH);
                return SKIP_BODY;
            default:
                pageContext.setAttribute(name, DEFAULT_TOP_JSP_PATH);
                return SKIP_BODY;
        }
    }
}
