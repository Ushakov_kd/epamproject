package com.web.audio.tag;

import com.web.audio.entity.Identifiable;

import javax.servlet.jsp.tagext.TagSupport;
import java.util.List;

public class PaginationVariablesTag<T extends Identifiable> extends TagSupport {

    private static final String START_ELEMENT_NAME = "startElement";
    private static final String END_ELEMENT_NAME = "endElement";

    private static final String NUMBER_OF_ENTITIES = "entitiesNumber";
    private static final String PAGINATION_CAPACITY = "capacity";
    private static final String PAGE_ITERATOR = "pageIterator";

    private static final String ITERATOR_PATTERN = "\\d{1,3}";

    private int capacity;
    private List<T> list;

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    @Override
    public int doStartTag(){

        int listSize;
        if(list == null) {
            listSize = 0;
        } else {
            listSize = list.size();
        }

        pageContext.setAttribute(NUMBER_OF_ENTITIES, listSize);

        pageContext.setAttribute(PAGINATION_CAPACITY, capacity);

        String currentIterator = pageContext.getRequest().getParameter(PAGE_ITERATOR);
        if(currentIterator == null || !currentIterator.matches(ITERATOR_PATTERN)) {
            currentIterator = "1";
        }
        pageContext.setAttribute(PAGE_ITERATOR, currentIterator);

        int startElement = (Integer.parseInt(currentIterator) - 1) * capacity;
        pageContext.setAttribute(START_ELEMENT_NAME, startElement);

        int endElement = startElement + capacity - 1;
        pageContext.setAttribute(END_ELEMENT_NAME, endElement);

        return SKIP_BODY;
    }
}
