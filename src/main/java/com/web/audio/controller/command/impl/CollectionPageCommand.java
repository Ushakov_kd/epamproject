package com.web.audio.controller.command.impl;

import com.web.audio.controller.command.Command;
import com.web.audio.controller.command.CommandResult;
import com.web.audio.controller.command.resource.PageManager;
import com.web.audio.entity.*;
import com.web.audio.service.AlbumService;
import com.web.audio.service.MusicCollectionService;
import com.web.audio.service.SongService;
import com.web.audio.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Optional;

public class CollectionPageCommand implements Command {

    private SongService songService;
    private MusicCollectionService collectionService;
    private AlbumService albumService;

    private static final String COLLECTION_ID_PARAMETER = "collection_id";
    private static final String COLLECTION_ATTRIBUTE = "collection";
    private static final String SONGS_ATTRIBUTE = "songs";
    private static final String TYPE_PARAMETER = "type";
    private static final String ALBUM_TYPE = "album";

    public CollectionPageCommand(SongService songService, MusicCollectionService collectionService, AlbumService albumService) {
        this.songService = songService;
        this.collectionService = collectionService;
        this.albumService = albumService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {

        String collectionId = request.getParameter(COLLECTION_ID_PARAMETER);
        String type = request.getParameter(TYPE_PARAMETER);

        boolean isAlbum = ALBUM_TYPE.equals(type);
        if(isAlbum){
            Optional<Album> collection = albumService.findAlbumById(collectionId);
            request.setAttribute(COLLECTION_ATTRIBUTE, collection.get());
        } else{
            Optional<MusicCollection> collection = collectionService.findCollectionById(collectionId);
            request.setAttribute(COLLECTION_ATTRIBUTE, collection.get());
        }

        List<Song> songs = songService.findCollectionSongs(collectionId);
        request.setAttribute(SONGS_ATTRIBUTE, songs);

        return new CommandResult(PageManager.COLLECTION_PAGE_PATH, true);
    }
}
