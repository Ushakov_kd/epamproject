package com.web.audio.controller.command.impl.admin;

import com.web.audio.controller.command.Command;
import com.web.audio.controller.command.CommandResult;
import com.web.audio.controller.command.resource.PageManager;
import com.web.audio.service.UserService;
import com.web.audio.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ChangeCustomerParametersCommand implements Command {

    private UserService userService;

    private static final String DISCOUNT_PARAMETER = "discount";
    private static final String BLOCK_ATTRIBUTE = "block";
    private static final String USER_ID_PARAMETER = "user_id";


    public ChangeCustomerParametersCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {

        String customerId = request.getParameter(USER_ID_PARAMETER);

        String discount = request.getParameter(DISCOUNT_PARAMETER);
        userService.changeUserDiscount(customerId, discount);

        String block = request.getParameter(BLOCK_ATTRIBUTE);
        userService.changeUserBlock(customerId, block);

        return new CommandResult(PageManager.REDIRECT_CUSTOMERS_PAGE_COMMAND, false);
    }
}
