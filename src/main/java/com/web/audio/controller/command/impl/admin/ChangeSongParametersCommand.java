package com.web.audio.controller.command.impl.admin;

import com.web.audio.controller.command.Command;
import com.web.audio.controller.command.CommandResult;
import com.web.audio.controller.command.resource.PageManager;
import com.web.audio.entity.Genre;
import com.web.audio.entity.Song;
import com.web.audio.entity.dto.SongDto;
import com.web.audio.service.ArtistService;
import com.web.audio.service.SongService;
import com.web.audio.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.Optional;

public class ChangeSongParametersCommand implements Command {

    private SongService songService;
    private ArtistService artistService;

    private static final String SONG_ID = "song_id";
    private static final String GENRE_PARAMETER = "genre";
    private static final String ARTIST_PARAMETER = "artist";
    private static final String TITLE_PARAMETER = "title";
    private static final String PRICE_PARAMETER = "price";

    private static final String MESSAGE_ATTRIBUTE = "message";
    private static final String SONG_ALREADY_EXISTS = "song_already_exists";
    private static final String FILL_IN_ALL_FIELDS = "fill_in_all_fields";
    private static final String UPGRADE_COMPLETED = "upgrade_completed";

    public ChangeSongParametersCommand(SongService songService, ArtistService artistService) {
        this.songService = songService;
        this.artistService = artistService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {

        String artistName = request.getParameter(ARTIST_PARAMETER);
        String title = request.getParameter(TITLE_PARAMETER);
        String genreStr = request.getParameter(GENRE_PARAMETER);
        String priceStr = request.getParameter(PRICE_PARAMETER);

        String songId = request.getParameter(SONG_ID);
        String path = PageManager.REDIRECT_SONG_PAGE_COMMAND_WITHOUT_ID+songId;

        if(!(artistName.length()>0) || !(title.length()>0) || !(genreStr.length()>0) || !(priceStr.length()>0)){
            request.setAttribute(MESSAGE_ATTRIBUTE, FILL_IN_ALL_FIELDS);
            return new CommandResult(path, true);
        }

        Genre genre = Genre.valueOf(genreStr);
        BigDecimal price = new BigDecimal(priceStr);
        SongDto searchSong = new SongDto(artistName, title, genre, price);

        Optional<Song> found = songService.findSingleSongByDto(searchSong);
        if(found.isPresent()){
            request.setAttribute(MESSAGE_ATTRIBUTE, SONG_ALREADY_EXISTS);
            return new CommandResult(path, true);
        }

        int id = artistService.getArtistId(artistName);
        String artistId = String.valueOf(id);
        SongDto dto = new SongDto(songId, artistId, title, genre, price);

        songService.changeAllSongParameters(dto);

        request.setAttribute(MESSAGE_ATTRIBUTE, UPGRADE_COMPLETED);
        return new CommandResult(path, true);
    }
}
