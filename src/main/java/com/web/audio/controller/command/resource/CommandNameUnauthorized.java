package com.web.audio.controller.command.resource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommandNameUnauthorized {

    public static final String START_PAGE_COMMAND = "start_page";
    public static final String LOGIN_PAGE_COMMAND = "login_page";
    public static final String LOGIN_COMMAND = "login";
    public static final String SET_LOCALE_COMMAND = "set_locale";

    public static List<String> getNamesAsList() {
        return new ArrayList<>(Arrays.asList(START_PAGE_COMMAND, LOGIN_PAGE_COMMAND,
                LOGIN_COMMAND, SET_LOCALE_COMMAND));
    }
}
