package com.web.audio.controller.command.resource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommandNameAuthorized {

    public static final String LOGOUT = "logout";
    public static final String SEARCH_PAGE = "search_page";
    public static final String SEARCH_SONG = "search_song";
    public static final String SEARCH_ALBUM = "search_album";
    public static final String SEARCH_COLLECTION = "search_collection";
    public static final String SONG_PAGE = "song_page";
    public static final String SET_PAGINATION_ITERATOR = "set_pagination_iterator";
    public static final String SET_LOCALE = "set_locale";

    public static List<String> getNamesAsList() {
        return new ArrayList<>(Arrays.asList(LOGOUT, SEARCH_PAGE, SEARCH_SONG, SEARCH_ALBUM,SEARCH_COLLECTION,
                SONG_PAGE, SET_PAGINATION_ITERATOR, SET_LOCALE));
    }
}
