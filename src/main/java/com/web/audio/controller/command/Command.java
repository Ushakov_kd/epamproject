package com.web.audio.controller.command;

import com.web.audio.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface Command {

    /**
     *
     * @param request to provide
     *  * request information for HTTP servlets.
     * @param response  to provide HTTP-specific
     *  * functionality in sending a response.
     * @return a new <code>CommandResult</code> which contain
     * path for redirect or forward with boolean isForward.
     * @throws ServiceException
     */
    CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException;
}
