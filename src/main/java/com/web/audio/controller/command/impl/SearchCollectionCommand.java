package com.web.audio.controller.command.impl;

import com.web.audio.controller.command.Command;
import com.web.audio.controller.command.CommandResult;
import com.web.audio.controller.command.resource.PageManager;
import com.web.audio.entity.Genre;
import com.web.audio.entity.MusicCollection;
import com.web.audio.entity.dto.MusicCollectionDto;
import com.web.audio.service.MusicCollectionService;
import com.web.audio.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class SearchCollectionCommand implements Command {

    private MusicCollectionService musicCollectionService;

    private static final String GENRE_PARAMETER = "genre";
    private static final String TITLE_PARAMETER = "title";

    private static final String COLLECTIONS_ATTRIBUTE = "collections";

    public SearchCollectionCommand(MusicCollectionService musicCollectionService) {
        this.musicCollectionService = musicCollectionService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {

        String title = request.getParameter(TITLE_PARAMETER);
        String genre = request.getParameter(GENRE_PARAMETER);

        MusicCollectionDto collectionDto = createCollectionDto(title, genre);
        List<MusicCollection> collections = musicCollectionService.findCollectionsByVariableNumberParameters(collectionDto);

        request.setAttribute(COLLECTIONS_ATTRIBUTE, collections);
        return new CommandResult(PageManager.COLLECTION_PAGE_PATH, true);
    }

    private MusicCollectionDto createCollectionDto(String title, String genreStr) {
        MusicCollectionDto collectionDto = new MusicCollectionDto();

        if (title.length() > 0) {
            collectionDto.setTitle(title);
        }
        if (genreStr.length() > 0) {
            Genre genre = Genre.valueOf(genreStr);
            collectionDto.setGenre(genre);
        }
        return collectionDto;
    }

}
