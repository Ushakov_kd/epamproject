package com.web.audio.controller.command.impl.customer;

import com.web.audio.controller.command.Command;
import com.web.audio.controller.command.CommandResult;
import com.web.audio.service.SongService;
import com.web.audio.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class OrderSongsCommand implements Command {

    private SongService songService;

    private static final String SERVLET_NAME = "Controller";
    private static final String SPLITTER = "?";
    private static final String PREVIOUS_QUERY_PARAMETER = "previousQuery";

    private static final String ORDERED_ID_SONGS_ATTRIBUTE = "buffered_id_songs";

    private static final String SONG_ID_PARAMETER = "song_id";
    private static final String USER_ID_PARAMETER = "user_id";

    public OrderSongsCommand(SongService songService) {
        this.songService = songService;
    }

    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {

        String[] idSongs = request.getParameterValues(SONG_ID_PARAMETER);

        String previousQuery = request.getParameter(PREVIOUS_QUERY_PARAMETER);
        String path = SERVLET_NAME+SPLITTER+previousQuery;

        if(idSongs == null) {
            return new CommandResult(path, true);
        }

        HttpSession session = request.getSession(false);
        List<String> order = (List<String>) session.getAttribute(ORDERED_ID_SONGS_ATTRIBUTE);

        String userId = request.getParameter(USER_ID_PARAMETER);

        for (String songId : idSongs) {
            if(isOrdered(order, songId)){
                continue;
            }
            if (songService.hasCustomerTheSong(userId, songId)) {
                continue;
            }
            order.add(songId);
        }
        session.setAttribute(ORDERED_ID_SONGS_ATTRIBUTE, order);
        return new CommandResult(path, true);
    }

    private boolean isOrdered(List<String> order, String addingSongId) {
        for (String orderedSong : order) {
            if (addingSongId.equals(orderedSong)) {
                return true;
            }
        }
        return false;
    }
}