package com.web.audio.controller.command.impl.customer;

import com.web.audio.controller.command.Command;
import com.web.audio.controller.command.CommandResult;
import com.web.audio.controller.command.resource.PageManager;
import com.web.audio.entity.Song;
import com.web.audio.service.SongService;
import com.web.audio.service.UserService;
import com.web.audio.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class CustomerMusicPageCommand implements Command {

    private SongService songService;
    private UserService userService;

    private static final String USER_ID_PARAMETER = "user_id";
    private static final String USER_SONGS_ATTRIBUTE = "user_songs";

    public CustomerMusicPageCommand(SongService songService, UserService userService) {
        this.songService = songService;
        this.userService = userService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {

        String userId = request.getParameter(USER_ID_PARAMETER);

        List<Song> songs = songService.findUserSongsByUserId(userId);

        request.setAttribute(USER_SONGS_ATTRIBUTE, songs);

        return new CommandResult(PageManager.CUSTOMER_MUSIC_PAGE_PATH, true);
    }
}
