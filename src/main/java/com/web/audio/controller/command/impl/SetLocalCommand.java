package com.web.audio.controller.command.impl;

import com.web.audio.controller.command.Command;
import com.web.audio.controller.command.CommandResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SetLocalCommand implements Command {

    private static final String SERVLET_NAME = "Controller";
    private static final String SPLITTER = "?";
    private static final String PREVIOUS_QUERY_PARAMETER = "previousQuery";

    private static final String LOCALE_ATTRIBUTE = "locale";
    private static final String LOCALE_PARAMETER = "locale";


    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response){

        String targetLocale = request.getParameter(LOCALE_PARAMETER);

        HttpSession session = request.getSession(false);
        session.setAttribute(LOCALE_ATTRIBUTE, targetLocale);

        String previousQuery = request.getParameter(PREVIOUS_QUERY_PARAMETER);
        String path = SERVLET_NAME+SPLITTER+previousQuery;

        return new CommandResult(path, true);
    }
}
