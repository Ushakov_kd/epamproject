package com.web.audio.controller.command.impl.customer;

import com.web.audio.controller.command.Command;
import com.web.audio.controller.command.CommandResult;
import com.web.audio.controller.command.resource.PageManager;
import com.web.audio.entity.Song;
import com.web.audio.service.SongService;
import com.web.audio.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.List;

public class OrderPageCommand implements Command {

    private SongService songService;

    private static final String ORDERED_ID_SONGS_ATTRIBUTE = "buffered_id_songs";
    private static final String ORDERED_SONGS_ATTRIBUTE = "ordered_songs";
    private static final String ORDER_TOTAL_PRICE_ATTRIBUTE = "order_total_price";

    public OrderPageCommand(SongService songService) {
        this.songService = songService;
    }

    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {

        HttpSession session = request.getSession(false);
        List<String> orderedSongsId = (List<String>) session.getAttribute(ORDERED_ID_SONGS_ATTRIBUTE);

        List<Song> orderedSongs = songService.findSongsById(orderedSongsId);
        request.setAttribute(ORDERED_SONGS_ATTRIBUTE, orderedSongs);

        BigDecimal orderPriceTotal = songService.calculateTotalPrice(orderedSongs);
        request.setAttribute(ORDER_TOTAL_PRICE_ATTRIBUTE, orderPriceTotal);

        return new CommandResult(PageManager.CUSTOMER_ORDER_PAGE_PATH, true);
    }
}
