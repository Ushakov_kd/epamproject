package com.web.audio.controller.command.impl.admin;

import com.web.audio.controller.command.Command;
import com.web.audio.controller.command.CommandResult;
import com.web.audio.controller.command.resource.PageManager;
import com.web.audio.entity.Album;
import com.web.audio.entity.Genre;
import com.web.audio.entity.dto.AlbumDto;
import com.web.audio.service.AlbumService;
import com.web.audio.service.ArtistService;
import com.web.audio.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

public class CreateAlbumCommand implements Command {

    private AlbumService albumService;
    private ArtistService artistService;

    private static final String GENRE_PARAMETER = "genre";
    private static final String ARTIST_PARAMETER = "artist";
    private static final String TITLE_PARAMETER = "title";

    private static final String MESSAGE_ATTRIBUTE = "message";
    private static final String ALBUM_ALREADY_EXISTS = "album_already_exists";
    private static final String FILL_IN_ALL_FIELDS = "fill_in_all_fields";

    public CreateAlbumCommand(AlbumService albumService, ArtistService artistService) {
        this.albumService = albumService;
        this.artistService = artistService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {

        String artistName = request.getParameter(ARTIST_PARAMETER);
        String title = request.getParameter(TITLE_PARAMETER);
        String genreStr = request.getParameter(GENRE_PARAMETER);

        if(!(artistName.length()>0) || !(title.length()>0) || !(genreStr.length()>0)){
            request.setAttribute(MESSAGE_ATTRIBUTE, FILL_IN_ALL_FIELDS);
            return new CommandResult(PageManager.CREATING_PAGE, true);
        }
        Genre genre = Genre.valueOf(genreStr);

        AlbumDto creatingAlbumData = new AlbumDto(artistName, title, genre);

        Optional<Album> foundAlbum = albumService.findSingleAlbumByDto(creatingAlbumData);
        if (foundAlbum.isPresent()) {
            request.setAttribute(MESSAGE_ATTRIBUTE, ALBUM_ALREADY_EXISTS);
            return new CommandResult(PageManager.CREATING_PAGE, true);
        }
        int id = artistService.getArtistId(artistName);
        String artistId = String.valueOf(id);
        AlbumDto creatingAlbum = new AlbumDto(artistId, title, genre);

        albumService.createAlbum(creatingAlbum);

        Optional<Album> optionalAlbum = albumService.findSingleAlbumByDto(creatingAlbumData);
        if(optionalAlbum.isEmpty()){
            return new CommandResult(PageManager.ERROR_PAGE_PATH, true);
        }
        Album album = optionalAlbum.get();

        String path = PageManager.REDIRECT_ALBUM_PAGE_ADMIN_COMMAND_WITHOUT_ID + album.getId();
        return new CommandResult(path, false);
    }
}