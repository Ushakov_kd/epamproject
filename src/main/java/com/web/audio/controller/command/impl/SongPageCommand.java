package com.web.audio.controller.command.impl;

import com.web.audio.controller.command.Command;
import com.web.audio.controller.command.CommandResult;
import com.web.audio.controller.command.resource.PageManager;
import com.web.audio.entity.Comment;
import com.web.audio.entity.Song;
import com.web.audio.service.CommentService;
import com.web.audio.service.SongService;
import com.web.audio.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Optional;

public class SongPageCommand implements Command {

    private CommentService commentService;
    private SongService songService;

    private static final String SONG_ID_PARAMETER = "song_id";
    private static final String COMMENTS_ATTRIBUTE = "comments";
    private static final String SONG_ATTRIBUTE = "song";

    public SongPageCommand( SongService songService, CommentService commentService) {
        this.commentService = commentService;
        this.songService = songService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {

        String songId = request.getParameter(SONG_ID_PARAMETER);

        Optional<Song> optionalSong = songService.findSongById(songId);
        if(optionalSong.isEmpty()){
            return new CommandResult(PageManager.ERROR_PAGE_PATH, true);
        }
        Song song = optionalSong.get();
        request.setAttribute(SONG_ATTRIBUTE, song);

        List<Comment> comments = commentService.findSongCommentsBySongId(songId);
        request.setAttribute(COMMENTS_ATTRIBUTE, comments);

        return new CommandResult(PageManager.SONG_PAGE_PATH, true);
    }
}
