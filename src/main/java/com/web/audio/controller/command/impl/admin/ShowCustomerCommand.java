package com.web.audio.controller.command.impl.admin;

import com.web.audio.controller.command.Command;
import com.web.audio.controller.command.CommandResult;
import com.web.audio.controller.command.resource.PageManager;
import com.web.audio.entity.User;
import com.web.audio.service.UserService;
import com.web.audio.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Optional;

public class ShowCustomerCommand implements Command {

    private UserService userService;

    private static final String USERS_ATTRIBUTE = "users";
    private static final String USER_ID_PARAMETER = "user_id";
    private static final String CUSTOMER_PARAMETER = "customer";

    public ShowCustomerCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {

        List<User> songs = userService.getAllCustomers();
        request.setAttribute(USERS_ATTRIBUTE, songs);

        String userId = request.getParameter(USER_ID_PARAMETER);
        Optional<User> user = userService.findUserById(userId);
        if(user.isEmpty()){
            return new CommandResult(PageManager.ERROR_PAGE_PATH, true);
        }
        request.setAttribute(CUSTOMER_PARAMETER, user.get());

        return new CommandResult(PageManager.CUSTOMERS_PAGE_PATH, true);
    }
}
