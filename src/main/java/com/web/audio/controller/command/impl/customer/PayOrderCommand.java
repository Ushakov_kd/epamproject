package com.web.audio.controller.command.impl.customer;

import com.web.audio.controller.command.Command;
import com.web.audio.controller.command.CommandResult;
import com.web.audio.controller.command.resource.PageManager;
import com.web.audio.service.SongService;
import com.web.audio.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

public class PayOrderCommand implements Command {

    private static final String USER_ID_PARAMETER = "user_id";
    private static final String ORDERED_ID_SONGS_ATTRIBUTE = "buffered_id_songs";

    private SongService songService;

    public PayOrderCommand(SongService songService) {
        this.songService = songService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {

        String userId = request.getParameter(USER_ID_PARAMETER);

        HttpSession session = request.getSession(false);
        List<String> orderedSongs = (List<String>)session.getAttribute(ORDERED_ID_SONGS_ATTRIBUTE);

        songService.addPaidSongsToUser(userId, orderedSongs);

        session.setAttribute(ORDERED_ID_SONGS_ATTRIBUTE, new ArrayList<String>());

        return new CommandResult(PageManager.REDIRECT_CUSTOMER_MUSIC_PAGE_COMMAND_WITHOUT_ID+userId, false);
    }
}
