package com.web.audio.controller.command.impl;

import com.web.audio.controller.command.Command;
import com.web.audio.controller.command.CommandResult;
import com.web.audio.controller.command.resource.PageManager;
import com.web.audio.entity.Genre;
import com.web.audio.entity.Song;
import com.web.audio.entity.dto.SongDto;
import com.web.audio.service.SongService;
import com.web.audio.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class SearchSongCommand implements Command {

    private SongService songService;

    private static final String GENRE_PARAMETER = "genre";
    private static final String ARTIST_PARAMETER = "artist";
    private static final String TITLE_PARAMETER = "title";

    private static final String SONGS_ATTRIBUTE = "songs";

    public SearchSongCommand(SongService songService) {
        this.songService = songService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {

        String artistName = request.getParameter(ARTIST_PARAMETER);
        String title = request.getParameter(TITLE_PARAMETER);
        String genreStr = request.getParameter(GENRE_PARAMETER);

        SongDto songDto = createSongDto(artistName, title, genreStr);
        List<Song> songs = songService.findSongsByVariableNumberParameters(songDto);

        request.setAttribute(SONGS_ATTRIBUTE, songs);
        return new CommandResult(PageManager.COLLECTION_PAGE_PATH, true);
    }

    private SongDto createSongDto(String artist, String title, String genreStr) {

        SongDto songDto = new SongDto();
        if (artist.length() > 0) {
            songDto.setArtist(artist);
        }
        if (title.length() > 0) {
            songDto.setTitle(title);
        }
        if (genreStr.length() > 0) {
            Genre genre = Genre.valueOf(genreStr);
            songDto.setGenre(genre);
        }
        return songDto;
    }
}
