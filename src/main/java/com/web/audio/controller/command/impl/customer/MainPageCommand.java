package com.web.audio.controller.command.impl.customer;

import com.web.audio.controller.command.Command;
import com.web.audio.controller.command.CommandResult;
import com.web.audio.controller.command.resource.PageManager;
import com.web.audio.entity.Song;
import com.web.audio.service.SongService;
import com.web.audio.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class MainPageCommand implements Command {

    private SongService songService;

    private static final String SONGS_ATTRIBUTE = "songs";
    private static final String PAGE_ITERATOR_ATTRIBUTE = "pageIterator";

    public MainPageCommand(SongService songService) {
        this.songService = songService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {

        List<Song> songs = songService.getAllSongs();

        request.setAttribute(SONGS_ATTRIBUTE, songs);

        String iteratorParameter = request.getParameter(PAGE_ITERATOR_ATTRIBUTE);
        if(iteratorParameter != null) {
            Integer pageIterator = Integer.parseInt(iteratorParameter);
            request.setAttribute(PAGE_ITERATOR_ATTRIBUTE, pageIterator);
        }
        return new CommandResult(PageManager.COLLECTION_PAGE_PATH, true);
    }
}
