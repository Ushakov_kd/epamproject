package com.web.audio.controller.command;

public class CommandResult {

    private String path;
    private boolean isForward;

    public CommandResult(){}

    public CommandResult(boolean isForward){
        this.isForward = isForward;
    }

    public CommandResult(String path, boolean isForward) {
        this.path = path;
        this.isForward = isForward;
    }

    public boolean isForward() {
        return isForward;
    }

    public String getPath() {
        return path;
    }
}
