package com.web.audio.controller.command.impl;

import com.web.audio.controller.command.Command;
import com.web.audio.controller.command.CommandResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GoToPageCommand implements Command {

    private String pagePath;

    public GoToPageCommand(String page) {
        this.pagePath = page;
    }

    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) {
        return new CommandResult(pagePath, true);
    }
}
