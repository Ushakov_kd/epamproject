package com.web.audio.controller.command.resource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommandNameCustomer {

    public static final String MAIN_PAGE = "customer_main_page";
    public static final String ORDER_PAGE = "customer_order_page";
    public static final String REDIRECT_TO_CUSTOMER_ORDER_PAGE = "redirect_to_customer_order_page";
    public static final String ORDER = "order";
    public static final String DELETE_ORDERED_SONG = "delete_ordered_song";
    public static final String PAY_ORDER = "pay_order";
    public static final String PERSONAL_MUSIC_PAGE = "customer_music_page";
    public static final String ADD_COMMENT = "add_comment";
    public static final String DELETE_USER_SONG = "delete_user_song";
    public static final String COLLECTION_PAGE = "collection_page";

    public static List<String> getNamesAsList() {
        return new ArrayList<>(Arrays.asList(MAIN_PAGE,
                ORDER_PAGE, REDIRECT_TO_CUSTOMER_ORDER_PAGE,
                ORDER, DELETE_ORDERED_SONG, PAY_ORDER,
                PERSONAL_MUSIC_PAGE, ADD_COMMENT, DELETE_USER_SONG, COLLECTION_PAGE));
    }

}
