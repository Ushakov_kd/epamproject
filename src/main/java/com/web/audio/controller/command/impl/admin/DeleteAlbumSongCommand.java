package com.web.audio.controller.command.impl.admin;

import com.web.audio.controller.command.Command;
import com.web.audio.controller.command.CommandResult;
import com.web.audio.controller.command.resource.PageManager;
import com.web.audio.service.SongService;
import com.web.audio.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DeleteAlbumSongCommand implements Command {

    private SongService songService;

    private static final String COLLECTION_ID = "collection_id";
    private static final String SONG_ID_PARAMETER = "song_id";

    public DeleteAlbumSongCommand(SongService songService) {
        this.songService = songService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {

        String collectionId = request.getParameter(COLLECTION_ID);
        String songId = request.getParameter(SONG_ID_PARAMETER);

        songService.deleteSongFromCollection(collectionId, songId);

        return new CommandResult(PageManager.REDIRECT_ALBUM_PAGE_ADMIN_COMMAND_WITHOUT_ID+collectionId, false);
    }
}
