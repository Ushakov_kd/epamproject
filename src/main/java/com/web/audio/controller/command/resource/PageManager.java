package com.web.audio.controller.command.resource;

public class PageManager {

    public static final String START_PAGE_PATH = "/WEB-INF/jsp/start_page.jsp";
    public static final String REDIRECT_START_PAGE_COMMAND = "Controller?command=start_page";

    public static final String LOGIN_PAGE_PATH = "/WEB-INF/jsp/login_page.jsp";
    public static final String REDIRECT_LOGIN_PAGE_COMMAND = "Controller?command=login_page";

    public static final String SONG_PAGE_PATH = "/WEB-INF/jsp/song_page.jsp";
    public static final String REDIRECT_SONG_PAGE_COMMAND_WITHOUT_ID = "Controller?command=song_page&song_id=";

    public static final String COLLECTION_PAGE_PATH = "/WEB-INF/jsp/collection_page.jsp";
    public static final String REDIRECT_ALBUM_PAGE_ADMIN_COMMAND_WITHOUT_ID = "Controller?command=admin_collection_page&type=album&collection_id=";
    public static final String REDIRECT_COLLECTION_PAGE_ADMIN_COMMAND_WITHOUT_ID = "Controller?command=admin_collection_page&type=collection&collection_id=";

    public static final String REDIRECT_CUSTOMER_MAIN_PAGE_COMMAND = "Controller?command=customer_main_page";

    public static final String CUSTOMER_ORDER_PAGE_PATH = "/WEB-INF/jsp/customer/order_page.jsp";
    public static final String REDIRECT_CUSTOMER_ORDER_PAGE_COMMAND = "Controller?command=customer_order_page";

    public static final String CUSTOMER_MUSIC_PAGE_PATH = "/WEB-INF/jsp/customer/personal_music_page.jsp";
    public static final String REDIRECT_CUSTOMER_MUSIC_PAGE_COMMAND_WITHOUT_ID = "Controller?command=customer_music_page&user_id=";

    public static final String SEARCH_PAGE_PATH = "/WEB-INF/jsp/search_page.jsp";

    public static final String CUSTOMERS_PAGE_PATH = "/WEB-INF/jsp/admin/customers_page.jsp";
    public static final String REDIRECT_CUSTOMERS_PAGE_COMMAND = "Controller?command=customers_page";

    public static final String CREATING_PAGE = "/WEB-INF/jsp/admin/create_page.jsp";

    public static final String ERROR_PAGE_PATH = "/WEB-INF/jsp/error_page.jsp";

}
