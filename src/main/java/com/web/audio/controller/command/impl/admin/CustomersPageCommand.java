package com.web.audio.controller.command.impl.admin;

import com.web.audio.controller.command.Command;
import com.web.audio.controller.command.CommandResult;
import com.web.audio.controller.command.resource.PageManager;
import com.web.audio.entity.User;
import com.web.audio.service.UserService;
import com.web.audio.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class CustomersPageCommand implements Command {

    private UserService userService;

    private static final String USERS_ATTRIBUTE = "users";

    public CustomersPageCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {

        List<User> songs = userService.getAllCustomers();
        request.setAttribute(USERS_ATTRIBUTE, songs);

        return new CommandResult(PageManager.CUSTOMERS_PAGE_PATH, true);
    }
}
