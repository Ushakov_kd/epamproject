package com.web.audio.controller.command.impl;

import com.web.audio.controller.command.Command;
import com.web.audio.controller.command.CommandResult;
import com.web.audio.controller.command.resource.PageManager;
import com.web.audio.entity.Album;
import com.web.audio.entity.Genre;
import com.web.audio.entity.dto.AlbumDto;
import com.web.audio.service.AlbumService;
import com.web.audio.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class SearchAlbumCommand implements Command {

    private AlbumService albumService;

    private static final String GENRE_PARAMETER = "genre";
    private static final String ARTIST_PARAMETER = "artist";
    private static final String TITLE_PARAMETER = "title";

    private static final String COLLECTIONS_ATTRIBUTE = "collections";

    public SearchAlbumCommand(AlbumService albumService) {
        this.albumService = albumService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {

        String artist = request.getParameter(ARTIST_PARAMETER);
        String title = request.getParameter(TITLE_PARAMETER);
        String genre = request.getParameter(GENRE_PARAMETER);

        AlbumDto album = createCollectionDto(artist, title, genre);
        List<Album> albums = albumService.findAlbumsByVariableNumberParameters(album);

        request.setAttribute(COLLECTIONS_ATTRIBUTE, albums);
        return new CommandResult(PageManager.COLLECTION_PAGE_PATH, true);
    }

    private AlbumDto createCollectionDto(String artist, String title, String genreStr) {
        AlbumDto albumDto = new AlbumDto();
        if (artist.length() > 0) {
            albumDto.setArtist(artist);
        }
        if (title.length() > 0) {
            albumDto.setTitle(title);
        }
        if (genreStr.length() > 0) {
            Genre genre = Genre.valueOf(genreStr);
            albumDto.setGenre(genre);
        }
        return albumDto;
    }
}
