package com.web.audio.controller.command.impl.admin;

import com.web.audio.controller.command.Command;
import com.web.audio.controller.command.CommandResult;

import com.web.audio.controller.command.resource.PageManager;
import com.web.audio.entity.Genre;
import com.web.audio.entity.MusicCollection;
import com.web.audio.entity.dto.MusicCollectionDto;
import com.web.audio.service.MusicCollectionService;
import com.web.audio.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

public class ChangeCollectionParametersCommand implements Command {

    private MusicCollectionService collectionService;

    private static final String COLLECTION_ID = "collection_id";
    private static final String GENRE_PARAMETER = "genre";
    private static final String TITLE_PARAMETER = "title";

    private static final String MESSAGE_ATTRIBUTE = "message";
    private static final String COLLECTION_ALREADY_EXISTS = "collection_already_exists";
    private static final String FILL_IN_ALL_FIELDS = "fill_in_all_fields";
    private static final String UPGRADE_COMPLETED = "upgrade_completed";

    public ChangeCollectionParametersCommand(MusicCollectionService collectionService) {
        this.collectionService = collectionService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {

        String title = request.getParameter(TITLE_PARAMETER);
        String genreStr = request.getParameter(GENRE_PARAMETER);

        String collectionId = request.getParameter(COLLECTION_ID);
        String path = PageManager.REDIRECT_COLLECTION_PAGE_ADMIN_COMMAND_WITHOUT_ID+collectionId;

        if (!(title.length() > 0) || !(genreStr.length() > 0)) {
            request.setAttribute(MESSAGE_ATTRIBUTE, FILL_IN_ALL_FIELDS);
            return new CommandResult(path, true);
        }

        Genre genre = Genre.valueOf(genreStr);
        MusicCollectionDto collectionDto = new MusicCollectionDto(title, genre);
        Optional<MusicCollection> foundCollection = collectionService.findSingleCollectionByDto(collectionDto);

        if (foundCollection.isPresent()) {
            request.setAttribute(MESSAGE_ATTRIBUTE, COLLECTION_ALREADY_EXISTS);
            return new CommandResult(path, true);
        }

        MusicCollectionDto dto = new MusicCollectionDto(collectionId, title, genre);
        collectionService.changeAllCollectionParameters(dto);

        request.setAttribute(MESSAGE_ATTRIBUTE, UPGRADE_COMPLETED);
        return new CommandResult(path, true);
    }
}
