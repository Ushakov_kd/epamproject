package com.web.audio.controller.command.resource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommandNameAdmin {

    public static final String CUSTOMERS_PAGE = "customers_page";
    public static final String SHOW_CUSTOMER = "show_customer";
    public static final String CHANGE_CUSTOMER = "change_customer";
    public static final String CREATING_PAGE = "create_page";
    public static final String CREATE_SONG = "create_song";
    public static final String CREATE_ALBUM = "create_album";
    public static final String CREATE_COLLECTION = "create_collection";
    public static final String ADD_SONG_TO_COLLECTION = "add_song_to_collection";
    public static final String DELETE_COLLECTION_SONG = "delete_collection_song";
    public static final String DELETE_ALBUM_SONG = "delete_album_song";
    public static final String CHANGE_SONG_PARAMETERS = "change_song_parameters";
    public static final String CHANGE_COLLECTION_PARAMETERS = "change_collection_parameters";
    public static final String CHANGE_ALBUM_PARAMETERS = "change_album_parameters";
    public static final String COLLECTION_PAGE = "admin_collection_page";


    public static List<String> getNamesAsList() {
        return new ArrayList<>(Arrays.asList(CUSTOMERS_PAGE, SHOW_CUSTOMER, CHANGE_CUSTOMER, CREATING_PAGE,
                CREATE_SONG, CREATE_ALBUM, CREATE_COLLECTION, DELETE_ALBUM_SONG,
                ADD_SONG_TO_COLLECTION, DELETE_COLLECTION_SONG, CHANGE_SONG_PARAMETERS, CHANGE_COLLECTION_PARAMETERS,
                CHANGE_ALBUM_PARAMETERS, COLLECTION_PAGE));
    }
}
