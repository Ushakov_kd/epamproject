package com.web.audio.controller.command.impl.admin;

import com.web.audio.controller.command.Command;
import com.web.audio.controller.command.CommandResult;
import com.web.audio.entity.Song;
import com.web.audio.entity.dto.SongDto;
import com.web.audio.service.SongService;
import com.web.audio.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

public class AddSongToCollectionCommand implements Command {

    private SongService songService;

    private static final String SERVLET_NAME = "Controller";
    private static final String QUERY_SPLITTER = "?";
    private static final String PREVIOUS_QUERY_PARAMETER = "previousQuery";

    private static final String COLLECTION_ID = "collection_id";

    private static final String SONG_NAME_PARAMETER = "song_name";
    private static final String SONG_NAME_SPLITTER = "-";
    private static final int ARTIST_NAME_INDEX = 0;
    private static final int TITLE_INDEX = 1;

    private static final String MESSAGE_ATTRIBUTE = "message";
    private static final String SONG_NOT_FOUND_MESSAGE = "song_not_found";
    private static final String SONG_IS_IN_COLLECTION_MESSAGE = "song_is_in_collection";

    public AddSongToCollectionCommand(SongService songService) {
        this.songService = songService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {

        String songName = request.getParameter(SONG_NAME_PARAMETER);
        String[] songParameters = songName.split(SONG_NAME_SPLITTER);

        String previousQuery = request.getParameter(PREVIOUS_QUERY_PARAMETER);
        String path = SERVLET_NAME+QUERY_SPLITTER+previousQuery;
        if(songParameters.length < 2) {
            request.setAttribute(MESSAGE_ATTRIBUTE, SONG_NOT_FOUND_MESSAGE);
            return new CommandResult(path, true);
        }

        String artistName = songParameters[ARTIST_NAME_INDEX].trim();
        String title = songParameters[TITLE_INDEX].trim();

        SongDto songDto = new SongDto(artistName, title);

        Optional<Song> optionalSong = songService.findSingleSongByDto(songDto);
        if(optionalSong.isEmpty()){
            request.setAttribute(MESSAGE_ATTRIBUTE, SONG_NOT_FOUND_MESSAGE);
            return new CommandResult(path, true);
        }

        Song song = optionalSong.get();
        int id = song.getId();
        String songId = String.valueOf(id);
        String collectionId = request.getParameter(COLLECTION_ID);

        boolean isSongInCollection = songService.hasCollectionTheSong(collectionId, songId);
        if(isSongInCollection){
            request.setAttribute(MESSAGE_ATTRIBUTE, SONG_IS_IN_COLLECTION_MESSAGE);
            return new CommandResult(path, true);
        }
        songService.addSongToCollection(collectionId, songId);
        return new CommandResult(path, false);
    }
}
