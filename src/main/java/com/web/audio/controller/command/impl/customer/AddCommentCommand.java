package com.web.audio.controller.command.impl.customer;

import com.web.audio.controller.command.Command;
import com.web.audio.controller.command.CommandResult;
import com.web.audio.controller.command.resource.PageManager;
import com.web.audio.service.CommentService;
import com.web.audio.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AddCommentCommand implements Command {

    private CommentService commentService;

    private static final String USER_ID_PARAMETER = "user_id";
    private static final String SONG_ID_PARAMETER = "song_id";
    private static final String TEXT_PARAMETER = "text";
    private static final String TEXT_PATTERN = ".{1,200}";

    public AddCommentCommand(CommentService commentService) {
        this.commentService = commentService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {

        String userId = request.getParameter(USER_ID_PARAMETER);
        String songId = request.getParameter(SONG_ID_PARAMETER);
        String commentText = request.getParameter(TEXT_PARAMETER);

        boolean isValidText = commentText.matches(TEXT_PATTERN);
        if (isValidText) {
            commentService.addComment(songId, userId, commentText);
        }
        String path = PageManager.REDIRECT_SONG_PAGE_COMMAND_WITHOUT_ID+songId;
        return new CommandResult(path, false);
    }
}
