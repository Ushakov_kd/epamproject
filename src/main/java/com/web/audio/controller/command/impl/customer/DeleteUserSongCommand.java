package com.web.audio.controller.command.impl.customer;

import com.web.audio.controller.command.Command;
import com.web.audio.controller.command.CommandResult;
import com.web.audio.controller.command.resource.PageManager;
import com.web.audio.service.SongService;
import com.web.audio.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DeleteUserSongCommand implements Command {

    private SongService songService;

    private static final String SONG_ID_PARAMETER = "song_id";
    private static final String USER_ID_PARAMETER = "user_id";

    public DeleteUserSongCommand(SongService songService) {
        this.songService = songService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {

        String userId = request.getParameter(USER_ID_PARAMETER);
        String songId = request.getParameter(SONG_ID_PARAMETER);

        songService.deleteSongFromUserSongs(userId, songId);

        return new CommandResult(PageManager.REDIRECT_CUSTOMER_MUSIC_PAGE_COMMAND_WITHOUT_ID+userId, false);
    }
}
