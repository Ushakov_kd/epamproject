package com.web.audio.controller.command.impl.customer;

import com.web.audio.controller.command.Command;
import com.web.audio.controller.command.CommandResult;
import com.web.audio.controller.command.resource.PageManager;
import com.web.audio.service.SongService;
import com.web.audio.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class DeleteOrderedSongCommand implements Command {

    private SongService songService;

    private static final String SONG_ID_PARAMETER = "song_id";
    private static final String ORDERED_ID_SONGS_ATTRIBUTE = "buffered_id_songs";

    public DeleteOrderedSongCommand(SongService songService) {
        this.songService = songService;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {

        String songId = request.getParameter(SONG_ID_PARAMETER);

        HttpSession session = request.getSession(false);
        List<String> orderedSongsId = (List<String>) session.getAttribute(ORDERED_ID_SONGS_ATTRIBUTE);

        orderedSongsId.remove(songId);
        session.setAttribute(ORDERED_ID_SONGS_ATTRIBUTE, orderedSongsId);

        return new CommandResult(PageManager.REDIRECT_CUSTOMER_ORDER_PAGE_COMMAND, false);
    }
}
