package com.web.audio.controller.command.impl;

import com.web.audio.controller.command.Command;
import com.web.audio.controller.command.CommandResult;
import com.web.audio.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SetPaginationIterator implements Command {

    private static final String PAGE_ITERATOR_ATTRIBUTE = "pageIterator";

    private static final String SERVLET_NAME = "Controller";
    private static final String SPLITTER = "?";
    private static final String PREVIOUS_QUERY_PARAMETER = "previousQuery";

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {

        String iteratorParameter = request.getParameter(PAGE_ITERATOR_ATTRIBUTE);
        Integer pageIterator = Integer.parseInt(iteratorParameter);
        request.setAttribute(PAGE_ITERATOR_ATTRIBUTE, pageIterator);

        String previousQuery = request.getParameter(PREVIOUS_QUERY_PARAMETER);
        String path = SERVLET_NAME+SPLITTER+previousQuery;

        return new CommandResult(path, true);
    }
}
