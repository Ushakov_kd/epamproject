package com.web.audio.controller.command.impl;

import com.web.audio.controller.command.Command;
import com.web.audio.controller.command.CommandResult;
import com.web.audio.controller.command.resource.PageManager;
import com.web.audio.entity.Genre;
import com.web.audio.entity.Song;
import com.web.audio.entity.User;
import com.web.audio.service.SongService;
import com.web.audio.service.UserService;
import com.web.audio.service.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.commons.codec.digest.DigestUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Optional;

public class LoginCommand implements Command {

    private static final Logger LOGGER = LogManager.getLogger(LoginCommand.class);

    private UserService userService;
    private SongService songService;

    private static final String LOGIN_PARAM_NAME = "login";
    private static final String PASSWORD_PARAM_NAME = "password";

    private static final String USER_ATTRIBUTE = "user";
    private static final String BUFFERED_SONGS_ATTRIBUTE = "buffered_id_songs";
    private static final String GENRES_ATTRIBUTE = "genres";

    private static final String MESSAGE_ATTRIBUTE = "message";
    private static final String USER_NOT_FOUND_MESSAGE = "user_not_found";
    private static final String USER_BLOCKED_MESSAGE = "user_blocked_message";
    private static final String UNEXPECTED_ROLE_MESSAGE = "Unexpected role";

    public LoginCommand(UserService userService, SongService songService) {
        this.userService = userService;
        this.songService = songService;
    }

    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException {

        String login = request.getParameter(LOGIN_PARAM_NAME);
        String password = request.getParameter(PASSWORD_PARAM_NAME);

        String hashPassword = DigestUtils.md5Hex(password);
        Optional<User> optionalUser = userService.login(login, hashPassword);

        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            HttpSession session = request.getSession(true);
            session.setAttribute(GENRES_ATTRIBUTE, Genre.values());
            switch (user.getRole()) {
                case CUSTOMER:
                    boolean block = user.isBlock();
                    if(block) {
                        request.setAttribute(MESSAGE_ATTRIBUTE, USER_BLOCKED_MESSAGE);
                        return new CommandResult(PageManager.LOGIN_PAGE_PATH, true);
                    }
                    session.setAttribute(USER_ATTRIBUTE, user);
                    session.setAttribute(BUFFERED_SONGS_ATTRIBUTE, new ArrayList<Song>());
                    return new CommandResult(PageManager.REDIRECT_CUSTOMER_MAIN_PAGE_COMMAND, false);
                case ADMIN:
                    session.setAttribute(USER_ATTRIBUTE, user);
                    return new CommandResult(PageManager.REDIRECT_CUSTOMERS_PAGE_COMMAND, false);
                default:
                    LOGGER.warn(UNEXPECTED_ROLE_MESSAGE);
                    return new CommandResult(PageManager.ERROR_PAGE_PATH, true);
            }
        }
        request.setAttribute(MESSAGE_ATTRIBUTE, USER_NOT_FOUND_MESSAGE);
        return new CommandResult(PageManager.LOGIN_PAGE_PATH, true);
    }
}
