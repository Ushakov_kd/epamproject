package com.web.audio.controller.command.impl;

import com.web.audio.controller.command.Command;
import com.web.audio.controller.command.CommandResult;
import com.web.audio.controller.command.resource.PageManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class LogoutCommand implements Command {

    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) {

        request.getSession().invalidate();
        return new CommandResult(PageManager.REDIRECT_START_PAGE_COMMAND, false);
    }
}
