package com.web.audio.controller;

import com.web.audio.controller.command.Command;
import com.web.audio.controller.command.CommandResult;
import com.web.audio.dao.DaoFactory;
import com.web.audio.dao.connection.ConnectionPool;
import com.web.audio.dao.connection.ProxyConnection;
import com.web.audio.service.ServiceFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Controller extends HttpServlet {

    private static final Logger LOGGER = LogManager.getLogger(Controller.class);
    private static final String COMMAND_PARAMETER = "command";
    private static final String ERROR_PAGE_PATH = "Error";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        performTask(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        performTask(request, response);
    }

    private void performTask(HttpServletRequest request, HttpServletResponse response) {

        ConnectionPool connectionPool = ConnectionPool.getInstance();

        try (ProxyConnection connection = connectionPool.getConnection()) {

            String commandName = request.getParameter(COMMAND_PARAMETER);

            DaoFactory daoFactory = new DaoFactory(connection);
            ServiceFactory serviceFactory = new ServiceFactory(daoFactory);
            CommandFactory factory = new CommandFactory(serviceFactory);

            Command command = factory.create(commandName);
            CommandResult result = command.execute(request, response);

            String pagePath = result.getPath();
            if (result.isForward()) {
                RequestDispatcher dispatcher = request.getRequestDispatcher(pagePath);
                dispatcher.forward(request, response);
            } else {
                response.sendRedirect(pagePath);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            try {
                response.sendRedirect(ERROR_PAGE_PATH);
            } catch (IOException ex) {
                LOGGER.error(ex.getMessage(), ex);
            }
        }
    }
}
