package com.web.audio.controller;

import com.web.audio.controller.command.Command;
import com.web.audio.controller.command.impl.*;
import com.web.audio.controller.command.impl.admin.*;
import com.web.audio.controller.command.impl.customer.*;
import com.web.audio.controller.command.resource.*;
import com.web.audio.service.ServiceFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CommandFactory {

    private ServiceFactory serviceFactory;

    private static final Logger LOGGER = LogManager.getLogger(CommandFactory.class);

    private static final String UNKNOWN_COMMAND_MESSAGE = "Unknown command: ";


    public CommandFactory(ServiceFactory factory) {
        this.serviceFactory = factory;
    }

    public Command create(String command) {
        switch (command) {
            case CommandNameAdmin.COLLECTION_PAGE:
                return new AdminCollectionPageCommand(serviceFactory.getSongService(),
                        serviceFactory.getMusicCollectionService(), serviceFactory.getAlbumService());
            case CommandNameAdmin.CHANGE_SONG_PARAMETERS:
                return new ChangeSongParametersCommand(serviceFactory.getSongService(), serviceFactory.getArtistService());
            case CommandNameAdmin.CHANGE_COLLECTION_PARAMETERS:
                return new ChangeCollectionParametersCommand(serviceFactory.getMusicCollectionService());
            case CommandNameAdmin.CHANGE_ALBUM_PARAMETERS:
                return new ChangeAlbumParametersCommand(serviceFactory.getAlbumService(), serviceFactory.getArtistService());
            case CommandNameAdmin.DELETE_ALBUM_SONG:
                return new DeleteAlbumSongCommand(serviceFactory.getSongService());
            case CommandNameAdmin.DELETE_COLLECTION_SONG:
                return new DeleteCollectionSongCommand(serviceFactory.getSongService());
            case CommandNameAdmin.ADD_SONG_TO_COLLECTION:
                return new AddSongToCollectionCommand(serviceFactory.getSongService());
            case CommandNameAdmin.CREATING_PAGE:
                return new GoToPageCommand(PageManager.CREATING_PAGE);
            case CommandNameAdmin.CREATE_SONG:
                return new CreateSongCommand(serviceFactory.getSongService(), serviceFactory.getArtistService());
            case CommandNameAdmin.CREATE_COLLECTION:
                return new CreateCollectionCommand(serviceFactory.getMusicCollectionService());
            case CommandNameAdmin.CREATE_ALBUM:
                return new CreateAlbumCommand(serviceFactory.getAlbumService(), serviceFactory.getArtistService());
            case CommandNameAdmin.CHANGE_CUSTOMER:
                return new ChangeCustomerParametersCommand(serviceFactory.getUserService());
            case CommandNameCustomer.DELETE_USER_SONG:
                return new DeleteUserSongCommand(serviceFactory.getSongService());
            case CommandNameAdmin.SHOW_CUSTOMER:
                return new ShowCustomerCommand(serviceFactory.getUserService());
            case CommandNameCustomer.ADD_COMMENT:
                return new AddCommentCommand(serviceFactory.getCommentService());
            case CommandNameAuthorized.SONG_PAGE:
                return new SongPageCommand(serviceFactory.getSongService(), serviceFactory.getCommentService());
            case CommandNameCustomer.COLLECTION_PAGE:
                return new CollectionPageCommand(serviceFactory.getSongService(),
                        serviceFactory.getMusicCollectionService(), serviceFactory.getAlbumService());
            case CommandNameAuthorized.SEARCH_PAGE:
                return new GoToPageCommand(PageManager.SEARCH_PAGE_PATH);
            case CommandNameAuthorized.SEARCH_SONG:
                return new SearchSongCommand(serviceFactory.getSongService());
            case CommandNameAuthorized.SEARCH_ALBUM:
                return new SearchAlbumCommand(serviceFactory.getAlbumService());
            case CommandNameAuthorized.SEARCH_COLLECTION:
                return new SearchCollectionCommand(serviceFactory.getMusicCollectionService());
            case CommandNameUnauthorized.START_PAGE_COMMAND:
                return new GoToPageCommand(PageManager.START_PAGE_PATH);
            case CommandNameUnauthorized.LOGIN_PAGE_COMMAND:
                return new GoToPageCommand(PageManager.LOGIN_PAGE_PATH);
            case CommandNameCustomer.MAIN_PAGE:
                return new MainPageCommand(serviceFactory.getSongService());
            case CommandNameCustomer.ORDER_PAGE:
                return new OrderPageCommand(serviceFactory.getSongService());
            case CommandNameCustomer.PERSONAL_MUSIC_PAGE:
                return new CustomerMusicPageCommand(serviceFactory.getSongService(), serviceFactory.getUserService());
            case CommandNameCustomer.REDIRECT_TO_CUSTOMER_ORDER_PAGE:
                return new GoToPageCommand(PageManager.CUSTOMER_ORDER_PAGE_PATH);
            case CommandNameUnauthorized.LOGIN_COMMAND:
                return new LoginCommand(serviceFactory.getUserService(), serviceFactory.getSongService());
            case CommandNameAuthorized.LOGOUT:
                return new LogoutCommand();
            case CommandNameCustomer.ORDER:
                return new OrderSongsCommand(serviceFactory.getSongService());
            case CommandNameCustomer.DELETE_ORDERED_SONG:
                return new DeleteOrderedSongCommand(serviceFactory.getSongService());
            case CommandNameCustomer.PAY_ORDER:
                return new PayOrderCommand(serviceFactory.getSongService());
            case CommandNameAuthorized.SET_LOCALE:
                return new SetLocalCommand();
            case CommandNameAuthorized.SET_PAGINATION_ITERATOR:
                return new SetPaginationIterator();
            case CommandNameAdmin.CUSTOMERS_PAGE:
                return new CustomersPageCommand(serviceFactory.getUserService());
            default:
                LOGGER.warn(UNKNOWN_COMMAND_MESSAGE + command);
                return new GoToPageCommand(PageManager.ERROR_PAGE_PATH);
        }
    }
}
