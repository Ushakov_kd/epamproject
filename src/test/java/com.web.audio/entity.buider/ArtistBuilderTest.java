package com.web.audio.entity.buider;

import com.web.audio.entity.Artist;
import com.web.audio.entity.Identifiable;
import com.web.audio.entity.builder.ArtistBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.sql.ResultSet;

public class ArtistBuilderTest {

    private static final int ID_INDEX = 1;
    private static final int ID = 1;
    private static final int NAME_INDEX = 2;
    private static final String NAME = "Fedor";

    private ArtistBuilder builder = new ArtistBuilder();
    private ResultSet resultSet = Mockito.mock(ResultSet.class);

    @Test
    public void testBuild() throws Exception {

        Mockito.when(resultSet.getInt(ID_INDEX)).thenReturn(ID);
        Mockito.when(resultSet.getString(NAME_INDEX)).thenReturn(NAME);
        Identifiable expected = new Artist(ID, NAME);

        Identifiable actual = builder.build(resultSet);

        Assert.assertEquals(expected, actual);
    }
}
